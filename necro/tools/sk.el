(require 'generic-x)


;; define your colors! in this file I am using the tuaregs defined ones 

;;(defface font-lock-necro-declaration-face '((t :foreground "#005f00")) "necro declaration keywords" :group 'necro-custon-mode)
;;(defface font-lock-necro-keywords-let-face '((t :foreground "#87875f")) "necro let keywords" :group 'necro-custon-mode)
;;(defface font-lock-necro-keywords-branch-face '((t :foreground "#87afd7")) "necro branch keywords" :group 'necro-custon-mode)
;;(defface font-lock-necro-symbols-face '((t :foreground "#af5f5f")) "necro symbols keywords" :group 'necro-custon-mode)
;;(defface font-lock-necro-constructors-face '((t :foreground "#d78787")) "necro constructors keywords" :group 'necro-custon-mode)
;;(defface font-lock-necro-terms-face '((t :foreground "#ffd7af")) "necro terms keywords" :group 'necro-custon-mode)
(require 'font-lock)

(setq necro-highlights
        `(
          (, (regexp-opt '("val" "type" "binder") 'symbols) . font-lock-type-face)
          (, (regexp-opt '("let" "in") 'symbols) . font-lock-keyword-face)
          (, (regexp-opt '("branch" "or" "end") 'symbols) . font-lock-keyword-face)
          (, "=" . font-lock-builtin-face)
          (, "(" . font-lock-builtin-face)
          (, ")" . font-lock-builtin-face)
          (, "->" . font-lock-builtin-face)
          (, "<-" . font-lock-builtin-face)
          (, "λ" . font-lock-builtin-face)
          (, ":" . font-lock-builtin-face)
          (, ";" . font-lock-builtin-face)
          (, "," . font-lock-builtin-face)
          (, "<" . font-lock-builtin-face)
          (, ">" . font-lock-builtin-face)
          (, "|" . font-lock-doc-face)
          (, "[.]+\\([a-z]\\|[A-Z]\\|[0-9]\\|\\(_\\)\\)+" . font-lock-warning-face) ;;accessors
          (, "\\([!?&*⊤⊥@$^∀∃+-/~]+[a-z]*[A-Z]*[0-9]*\\)+" . font-lock-builtin-face) ;;binders
          (, "%\\<[a-z]\\(\\([a-z]*[A-Z]*[0-9]*\\)\\(_\\)*\\)*\\_>" . font-lock-builtin-face) ;;%[fa-z]([A-Z]*[a-z]*[0-9]*_*)*
          ;;(, "\\_<\\([a-z]\\|\\(_\\)\\)\\([A-Z]*[a-z]*[0-9]*\\(_\\)*\\)*\\('\\)*\\_>" . font-lock-variable-name-face) ;;[a-z]([A-Z]*[a-z]*[0-9]*(_)*)*(')*
          (, "\\_<[A-Z]\\([A-Z]*[a-z]*[0-9]*\\(_\\)*\\)*\\_>" . font-lock-doc-face) ;;[A-Z]([A-Z]*[a-z]*[0-9]*(_)*)* 
          ))

(define-derived-mode necro-mode tuareg-mode "necro" "Major mode for editing necro"
  (setq font-lock-defaults '((necro-highlights))))

(add-to-list 'auto-mode-alist '("\\.sk\\'" . necro-mode))

;; including highlighting in org-mode
;;(after! org
  ;;(add-to-list 'org-src-lang-modes '("sk" . necro)))
