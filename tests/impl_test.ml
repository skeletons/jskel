open Test_monads

exception Fail of string

module ID = struct
	exception Branch_fail of string
	type 'a t = 'a
	let ret x = x
	let rec branch l =
		begin match l with
		| [] -> raise (Branch_fail "No branch matches")
		| b1 :: bq ->
				try b1 () with Branch_fail _ -> branch bq
		end
	let fail s = raise (Branch_fail s)
	let bind x f = f x
	let apply f x = f x
end

module Unspec = struct
  include Unspec(ID)(struct end)

  let myfail (v : unit) : 'a M.t = raise (Fail "JSkel failure due to monads")
end

module Test = MakeInterpreter(Unspec)

open Test

let _ =
  let (res,_) = test () {_Strict_ = F;_Foo_=()} in
  match res with
  | Success T -> print_endline "true"
  | Success F -> print_endline "false"
  | Anomaly _ -> print_endline "ouch"
