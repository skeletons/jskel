module Type = struct
  type nat = int
  type value = int
end

module ContPoly = struct
  type kind = | Anon | Spec of string | Unspec of string
  type 'b fcont = string -> 'b
  type ('a,'b) cont = 'a -> 'b fcont -> 'b
  type 'a t = {cont: 'b. (('a,'b) cont -> 'b fcont -> 'b)}
  let ret (x: 'a) = { cont= fun k fcont -> k x fcont }
  let bind (x: 'a t) (f: 'a -> 'b t) : 'b t =
    { cont = fun k fcont -> x.cont (fun v fcont' -> (f v).cont k fcont') fcont }
  let fail s = { cont = fun k fcont -> fcont s }
  let rec branch l = { cont = fun k fcont ->
    match l with
    | [] -> fcont "No branch matches"
    | b :: bs -> (b ()).cont k (fun _ -> (branch bs).cont k fcont) }
  let apply _ f x = f x
end

module Unspec = struct
  include Test_cont.Unspec (ContPoly) (Type)

  let plus v1 = ContPoly.ret (fun v2 -> ContPoly.ret (v1 + v2))
  let value_of_nat v = ContPoly.ret v
end

open (Test_cont.MakeInterpreter(Unspec))

let extract w = w.ContPoly.cont (fun v _ -> v) (fun s -> failwith s)

let t = Plus (Const 3, Const 5)

let res = extract (eval' t)

let _ = Printf.printf "%d" res
