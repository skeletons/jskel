(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type env
  type loc
  type nat
  type state
  type var
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: string -> 'a t
  type kind =
  | Anon
  | Spec of string
  | Unspec of string
  val apply: kind -> ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type value =
  | Susp of (value * cfun)
  | Return of pure
  | Cont of cfun
  and term =
  | Yield of term
  | Var of var
  | TNat of nat
  | Set of (term * term)
  | Seq of (term * term)
  | Run of term
  | Resume of (term * term)
  | Ref of term
  | Plus of (term * term)
  | Mult of (term * term)
  | LetIn of (var * term * term)
  | Lam of (var * term)
  | Get of term
  | Case of (term * term * term)
  and pure =
  | Unit
  | Nat of nat
  | Loc of loc
  | Clos of (var * term * env)
  and 'a pcstack =
  | Nil
  | Cons of ('a pcfun * 'a pcstack)
  and cfun = (state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cont = ((state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cstack = (state -> (state * value) M.t) pcstack
  and 'a pcfun = 'a -> ('a pcstack -> 'a M.t) M.t
  and 'a pcont = ('a -> ('a pcstack -> 'a M.t) M.t) -> ('a pcstack -> 'a M.t) M.t
  and 'a st = state -> (state * 'a) M.t
  and stv = state -> (state * value) M.t

  val alloc: state -> (value -> (state * loc) M.t) M.t
  val dbg: value -> unit M.t
  val extEnv: env -> (var -> (value -> env M.t) M.t) M.t
  val getEnv: env -> (var -> value M.t) M.t
  val mult: nat -> (nat -> nat M.t) M.t
  val plus: nat -> (nat -> nat M.t) M.t
  val st_read: state -> (loc -> value M.t) M.t
  val st_write: state -> (loc -> (value -> state M.t) M.t) M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type value =
  | Susp of (value * cfun)
  | Return of pure
  | Cont of cfun
  and term =
  | Yield of term
  | Var of var
  | TNat of nat
  | Set of (term * term)
  | Seq of (term * term)
  | Run of term
  | Resume of (term * term)
  | Ref of term
  | Plus of (term * term)
  | Mult of (term * term)
  | LetIn of (var * term * term)
  | Lam of (var * term)
  | Get of term
  | Case of (term * term * term)
  and pure =
  | Unit
  | Nat of nat
  | Loc of loc
  | Clos of (var * term * env)
  and 'a pcstack =
  | Nil
  | Cons of ('a pcfun * 'a pcstack)
  and cfun = (state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cont = ((state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cstack = (state -> (state * value) M.t) pcstack
  and 'a pcfun = 'a -> ('a pcstack -> 'a M.t) M.t
  and 'a pcont = ('a -> ('a pcstack -> 'a M.t) M.t) -> ('a pcstack -> 'a M.t) M.t
  and 'a st = state -> (state * 'a) M.t
  and stv = state -> (state * value) M.t

  let alloc _ = raise (NotImplemented "alloc")
  let dbg _ = raise (NotImplemented "dbg")
  let extEnv _ = raise (NotImplemented "extEnv")
  let getEnv _ = raise (NotImplemented "getEnv")
  let mult _ = raise (NotImplemented "mult")
  let plus _ = raise (NotImplemented "plus")
  let st_read _ = raise (NotImplemented "st_read")
  let st_write _ = raise (NotImplemented "st_write")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type env
  type loc
  type nat
  type state
  type var

  type value =
  | Susp of (value * cfun)
  | Return of pure
  | Cont of cfun
  and term =
  | Yield of term
  | Var of var
  | TNat of nat
  | Set of (term * term)
  | Seq of (term * term)
  | Run of term
  | Resume of (term * term)
  | Ref of term
  | Plus of (term * term)
  | Mult of (term * term)
  | LetIn of (var * term * term)
  | Lam of (var * term)
  | Get of term
  | Case of (term * term * term)
  and pure =
  | Unit
  | Nat of nat
  | Loc of loc
  | Clos of (var * term * env)
  and 'a pcstack =
  | Nil
  | Cons of ('a pcfun * 'a pcstack)
  and cfun = (state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cont = ((state -> (state * value) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t) -> ((state -> (state * value) M.t) pcstack -> (state -> (state * value) M.t) M.t) M.t
  and cstack = (state -> (state * value) M.t) pcstack
  and 'a pcfun = 'a -> ('a pcstack -> 'a M.t) M.t
  and 'a pcont = ('a -> ('a pcstack -> 'a M.t) M.t) -> ('a pcstack -> 'a M.t) M.t
  and 'a st = state -> (state * 'a) M.t
  and stv = state -> (state * value) M.t

  val alloc: state -> (value -> (state * loc) M.t) M.t
  val bind: cont -> ((value -> cont M.t) -> cont M.t) M.t
  val dbg: value -> unit M.t
  val eval: term -> (env -> cont M.t) M.t
  val extEnv: env -> (var -> (value -> env M.t) M.t) M.t
  val get: loc -> cont M.t
  val getEnv: env -> (var -> value M.t) M.t
  val id: (state -> (state * value) M.t) -> (cstack -> (state -> (state * value) M.t) M.t) M.t
  val init: term -> (env -> (state -> (state * value) M.t) M.t) M.t
  val mult: nat -> (nat -> nat M.t) M.t
  val newref: value -> cont M.t
  val plus: nat -> (nat -> nat M.t) M.t
  val ret: value -> cont M.t
  val set: loc -> (value -> cont M.t) M.t
  val st_read: state -> (loc -> value M.t) M.t
  val st_write: state -> (loc -> (value -> state M.t) M.t) M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let apply1 = M.apply
  let apply2 k f arg1 arg2 =
    let* _tmp = apply1 k f arg1 in
    apply1 M.Anon _tmp arg2
  let apply3 k f arg1 arg2 arg3 =
    let* _tmp = apply1 k f arg1 in
    apply2 M.Anon _tmp arg2 arg3
  let apply4 k f arg1 arg2 arg3 arg4 =
    let* _tmp = apply1 k f arg1 in
    apply3 M.Anon _tmp arg2 arg3 arg4

  let rec bind w =
    M.ret (function f ->
      M.ret (function k ->
        M.ret (function ks ->
          M.ret (function s ->
            apply3 M.Anon w (function st ->
              M.ret (function ks' ->
                M.ret (function s' ->
                  let* (s'', v) = apply1 M.Anon st s' in
                  let* w' = apply1 M.Anon f v in
                  apply3 M.Anon w' k ks' s''))) ks s))))
  and eval t =
    M.ret (function e ->
      begin match t with
      | Var x ->
          let* res = apply2 (M.Unspec "getEnv") getEnv e x in
          apply1 (M.Spec "ret") ret res
      | TNat n -> apply1 (M.Spec "ret") ret (Return (Nat n))
      | Lam (x, tx) -> apply1 (M.Spec "ret") ret (Return (Clos (x, tx, e)))
      | Plus (t1, t2) ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function _tmp ->
            begin match _tmp with
            | Return Nat n1 ->
                let* _tmp = apply2 (M.Spec "eval") eval t2 e in
                apply2 (M.Spec "bind") bind _tmp (function _tmp ->
                  begin match _tmp with
                  | Return Nat n2 ->
                      let* n = apply2 (M.Unspec "plus") plus n1 n2 in
                      apply1 (M.Spec "ret") ret (Return (Nat n))
                  | _ -> M.fail ""
                  end)
            | _ -> M.fail ""
            end)
      | Mult (t1, t2) ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function _tmp ->
            begin match _tmp with
            | Return Nat n1 ->
                let* _tmp = apply2 (M.Spec "eval") eval t2 e in
                apply2 (M.Spec "bind") bind _tmp (function _tmp ->
                  begin match _tmp with
                  | Return Nat n2 ->
                      let* n = apply2 (M.Unspec "mult") mult n1 n2 in
                      apply1 (M.Spec "ret") ret (Return (Nat n))
                  | _ -> M.fail ""
                  end)
            | _ -> M.fail ""
            end)
      | LetIn (x, tx, tc) ->
          let* _tmp = apply2 (M.Spec "eval") eval tx e in
          apply2 (M.Spec "bind") bind _tmp (function v ->
            let* e' = apply3 (M.Unspec "extEnv") extEnv e x v in
            apply2 (M.Spec "eval") eval tc e')
      | Seq (t1, t2) ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function _ ->
            apply2 (M.Spec "eval") eval t2 e)
      | Ref ti ->
          let* _tmp = apply2 (M.Spec "eval") eval ti e in
          apply2 (M.Spec "bind") bind _tmp (function v ->
            apply1 (M.Spec "newref") newref v)
      | Get ti ->
          let* _tmp = apply2 (M.Spec "eval") eval ti e in
          apply2 (M.Spec "bind") bind _tmp (function _tmp ->
            begin match _tmp with
            | Return Loc l -> apply1 (M.Spec "get") get l
            | _ -> M.fail ""
            end)
      | Set (tl, tv) ->
          let* _tmp = apply2 (M.Spec "eval") eval tl e in
          apply2 (M.Spec "bind") bind _tmp (function _tmp ->
            begin match _tmp with
            | Return Loc l ->
                let* _tmp = apply2 (M.Spec "eval") eval tv e in
                apply2 (M.Spec "bind") bind _tmp (function v ->
                  apply2 (M.Spec "set") set l v)
            | _ -> M.fail ""
            end)
      | Run t1 ->
          M.ret (function k ->
            M.ret (function ks ->
              apply4 (M.Spec "eval") eval t1 e id (Cons (k, ks))))
      | Yield t1 ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function v ->
            M.ret (function k ->
              M.ret (function ks ->
                begin match ks with
                | Cons (k', ks') ->
                    apply2 M.Anon k' (function s ->
                      M.ret (s, Susp (v, k))) ks'
                | _ -> M.fail ""
                end)))
      | Case (t1, t2, t3) ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function v1 ->
            begin match v1 with
            | Return _ ->
                let* _tmp = apply2 (M.Spec "eval") eval t2 e in
                apply2 (M.Spec "bind") bind _tmp (function _tmp ->
                  begin match _tmp with
                  | Return Clos (x, tx, ex) ->
                      let* ex1 = apply3 (M.Unspec "extEnv") extEnv ex x v1 in
                      apply2 (M.Spec "eval") eval tx ex1
                  | _ -> M.fail ""
                  end)
            | Susp (v', k') ->
                let* _tmp = apply2 (M.Spec "eval") eval t3 e in
                apply2 (M.Spec "bind") bind _tmp (function _tmp ->
                  begin match _tmp with
                  | Return Clos (x, tx, ex) ->
                      let* ex1 = apply3 (M.Unspec "extEnv") extEnv ex x v' in
                      let* _tmp = apply2 (M.Spec "eval") eval tx ex1 in
                      apply2 (M.Spec "bind") bind _tmp (function _tmp ->
                        begin match _tmp with
                        | Return Clos (y, ty, ey) ->
                            let* ey1 = apply3 (M.Unspec "extEnv") extEnv ey y (Cont k') in
                            apply2 (M.Spec "eval") eval ty ey1
                        | _ -> M.fail ""
                        end)
                  | _ -> M.fail ""
                  end)
            | _ -> M.fail ""
            end)
      | Resume (t1, t2) ->
          let* _tmp = apply2 (M.Spec "eval") eval t1 e in
          apply2 (M.Spec "bind") bind _tmp (function v1 ->
            let* _tmp = apply2 (M.Spec "eval") eval t2 e in
            apply2 (M.Spec "bind") bind _tmp (function v2 ->
              M.ret (function k ->
                M.ret (function ks ->
                  begin match v1 with
                  | Cont k' ->
                      apply2 M.Anon k' (function s ->
                        M.ret (s, v2)) (Cons (k, ks))
                  | _ -> M.fail ""
                  end))))
      end)
  and get l =
    M.ret (function k ->
      M.ret (function ks ->
        let st =
          function s ->
          let* v = apply2 (M.Unspec "st_read") st_read s l in
          M.ret (s, v)
        in
        apply2 M.Anon k st ks))
  and id f =
    M.ret (function ks ->
      M.ret (function s ->
        begin match ks with
        | Nil -> apply1 M.Anon f s
        | Cons (k, ks') -> apply3 M.Anon k f ks' s
        end))
  and init t =
    M.ret (function e ->
      let* w = apply2 (M.Spec "eval") eval t e in
      apply2 M.Anon w id Nil)
  and newref v =
    M.ret (function k ->
      M.ret (function ks ->
        let st =
          function s ->
          let* (s', l) = apply2 (M.Unspec "alloc") alloc s v in
          M.ret (s', Return (Loc l))
        in
        apply2 M.Anon k st ks))
  and ret v =
    M.ret (function k ->
      M.ret (function ks ->
        M.ret (function s ->
          apply3 M.Anon k (function s' ->
            M.ret (s', v)) ks s)))
  and set l =
    M.ret (function v ->
      M.ret (function k ->
        M.ret (function ks ->
          let st =
            function s ->
            let* s' = apply3 (M.Unspec "st_write") st_write s l v in
            M.ret (s', Return Unit)
          in
          apply2 M.Anon k st ks)))
end