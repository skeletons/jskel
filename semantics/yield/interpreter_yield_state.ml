open Yield_state

module SMap = Map.Make(String)
module IMap = Map.Make(struct type t = int let compare = compare end)

module rec Types : sig
  type var = string
  type nat = int
  type loc = int
  type state = (Unspec(Necromonads.ContPoly)(Types).value IMap.t) * int
  type env = Unspec(Necromonads.ContPoly)(Types).value SMap.t
end = Types

module Unspec = struct
  include Unspec(Necromonads.ContPoly)(Types)

  let getEnv e = M.ret (fun x -> M.ret (SMap.find x e))
  let extEnv e = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let plus a = M.ret (fun b -> M.ret (a + b))
  let mult a = M.ret (fun b -> M.ret (a * b))

  let alloc (h,i) = M.ret (fun v -> let h' = IMap.add i v h in M.ret ((h', i+1), i))
  let st_read (h,_) = M.ret (fun l -> let v = IMap.find l h in M.ret v)
  let st_write (h,i) = M.ret (fun l -> M.ret (fun v -> let h' = IMap.add l v h in M.ret (h',i)))

  let string_of_pure = function
    | Nat n -> string_of_int n
    | Clos _ -> "Closure"
    | Loc _ -> "Loc"
    | Unit -> "Unit"

  let string_of_value = function
    | Return p -> string_of_pure p
    | Susp _ -> "Susp"
    | Cont _ -> "Cont"

  let dbg v = Printf.printf "debug: %s\n" (string_of_value v); flush stdout; M.ret ()
end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let initial_env = SMap.empty
let initial_state = IMap.empty, 0

let run t =
  let _,v = M.extract (M.extract ((M.extract (init t)) initial_env) initial_state) in
  print_endline (string_of_value v)

let t = LetIn ("x", Run (Yield (Ref (TNat 12))),
                Case (Var "x",
                      Lam ("_", TNat 0),
                      Lam ("v", Lam ("k",
                                     Mult (Get (Var "v"), TNat 2)))))

let t0 = LetIn ("r", Ref (TNat 1), LetIn ("_", Set (Var "r", TNat 2), Get (Var "r")))

let t1 = LetIn ("x", Run (LetIn ("ref", Yield (Ref (TNat 12)),
                                 (Get (Var "ref")))),
                Case (Var "x",
                      Lam ("y", TNat 0),
                      Lam ("v", Lam ("k",
                                     LetIn ("_", Set (Var "v", Mult (Get (Var "v"), TNat 2)),
                                            Case (Resume(Var "k", Var "v"),
                                                  Lam ("y", Plus (Var "y", Get (Var "v"))),
                                                  Lam ("g", Lam ("gg", TNat (-1)))))))))

let t2 =
  LetIn ("r",
         Ref (TNat 2),
         LetIn ("x", Run (Seq (Seq (Yield (Get (Var "r")),
                                    Yield (Get (Var "r"))),
                               Get (Var "r"))),
                Case (Var "x",
                      Lam ("y", TNat 0),
                      Lam ("v1",
                           Lam ("k",
                                Seq (Set (Var "r", TNat 3),
                                     Case (Resume(Var "k", Var "v1"),
                                           Lam ("y", TNat 0),
                                           Lam ("v2",
                                                Lam ("k",
                                                     Seq (Set (Var "r", TNat 5),
                                                          Case (Resume(Var "k", Var "v1"),
                                                                Lam ("v3",
                                                                     Mult (Mult (Var "v1", Var "v2"), Var "v3")),
                                                                Lam ("g", Lam ("gg", TNat (-1))))))))))))))

let () = run t2
