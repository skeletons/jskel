open New_yield

module SMap = Map.Make(String)

module rec Types : sig
  type var = string
  type nat = int
  type env = Unspec(Necromonads.ContPoly)(Types).value SMap.t
end = Types

module Unspec = struct
  include Unspec(Necromonads.ContPoly)(Types)

  let emptyenv = SMap.empty

  let getEnv e = M.ret (fun x -> M.ret (SMap.find x e))
  let extEnv e = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let plus a = M.ret (fun b -> M.ret (a + b))
  let mult a = M.ret (fun b -> M.ret (a * b))

  let string_of_pure = function
    | Nat n -> string_of_int n
    | Clos _ -> "Closure"

  let string_of_value = function
    | Return p -> string_of_pure p
    | Susp _ -> "Susp"
    | Cont _ -> "Cont"

  let dbg v = Printf.printf "debug: %s\n" (string_of_value v); flush stdout; M.ret ()
end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let run t =
  let v = M.extract (init t) in
  print_endline (string_of_value v)

let t1 = LetIn ("x", Run (LetIn ("xx",
                                 Yield (TNat 1),
                                 LetIn ("xxx", Yield (Plus (Var "xx", TNat 1)), Plus(Var "xxx", TNat 2)))),
                Case (Var "x",
                      Lam("y", TNat 0),
                      Lam("v", Lam("k",
                                   Case (Resume(Var "k", Mult (Var "v", TNat 10)),
                                         Lam("y", TNat (-1)),
                                         Lam("vv", (Lam ("kk",
                                                         Case (Resume (Var "kk", Mult (Var "vv", TNat 100)),
                                                               Lam ("f", Var "f"),
                                                               Lam ("g", Lam ("gg", TNat (-3)))
                                                              )))))))))

let () = run t1
