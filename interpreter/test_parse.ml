#require "flow_parser";;
#load "jskel.cmo";;
#load "instance.cmo";;

open Instance
open Unspec
open InterpJs

let read_file (path : string) : string =
  let ch = open_in path in
  let s = really_input_string ch (in_channel_length ch) in
  close_in ch;
  s


let initialState (v  : Instance.InterpJs.value Instance.InterpJs.maybeEmpty Instance.InterpJs.out Instance.InterpJs.st) :(Instance.InterpJs.value Instance.InterpJs.maybeEmpty Instance.InterpJs.out * Instance.InterpJs.state) (*list*) =
  v {_StrictMode_ =  F;
     _ExecutionContextStack_ =  ([]);
     _ExecutionContexts_ =  (Instance.IntMap.empty, 0);
     _EnvironmentRecords_ =  (Instance.IntMap.empty, 0);
     _ModuleRecords_ =  (Instance.IntMap.empty, 0);
     _RealmRecords_ =  (Instance.IntMap.empty, 0);
     _ScriptRecords_  =  (Instance.IntMap.empty, 0);
     _Objects_ =  (Instance.IntMap.empty, 0);
     _LocalEnv_ = (Instance.IntMap.empty, 0);}

let _ =
    let code = read_file "../JS_tests/test.js" in
    let parser = Parser_flow.program in
    let parse_options =
      Some
        Parser_env.
      {
        enums = false;
        esproposal_class_instance_fields = false;
        esproposal_class_static_fields = false;
        esproposal_decorators = false;
        esproposal_export_star_as = false;
        esproposal_optional_chaining = false;
        esproposal_nullish_coalescing = false;
        types = false;
        use_strict = false;
      } in
    let ((_, ast), _) = parser ~parse_options code ~fail:false  in ast

let _ =
    let code = read_file "../JS_tests/test 2.js" in
    parseText code

let _ =
  let code = read_file "../JS_tests/test 2.js" in
  let (res,s) = initialState (InterpJs.runJS code) in
  let (os,_) = s._Objects_ in
  let (es,_) = s._EnvironmentRecords_ in
  let (excs,_) = s._ExecutionContexts_ in
  (*let args_obj = IntMap.find 18 os in
  let o_fun = IntMap.find 16 os in
  let o_gen = IntMap.find 22 os in
  let exc_fun = IntMap.find 4 excs in
  let lex_env = IntMap.find 6 es in
  let var_env = IntMap.find 5 es in
  (*let (ers,_) = s._LocalEnv_ in (*res, IntMap.find 22 ers*)
  let (es,_) = s._Objects_  in
  let (r,s') = InterpJs.resolveBinding ("blabla", NotEmpty (MU_Defined 3)) s in
  (*res,IntMap.fold (fun _ e acc -> (match e with
                                   | Test _ -> e::acc
                                   | Test_Bool _ -> e::acc
                                   | Test_BID _ -> e::acc
                                   | Test_EC _ -> e::acc
                                   | Test_ECR _ -> e::acc
                                   | Test_IR _ -> e::acc
                                   (*| Test_CR_Obj _ -> e::acc*)
                                   | Test_CR_Val _ -> e::acc
                                   (*| Test_CR_ME_Val _ -> e::acc*)
                                   (*| Test_Out_VR _ -> e::acc*)
                                   | Test_GS _ -> e::acc
                                   | Test_Sync _ -> e::acc
                                   | Test_List_PN _ -> e::acc
                                   | Test_STK _ -> e::acc
                                   | Test_List_Val _ -> e::acc
                                   | Test_list_Str _ -> e::acc
                                   | Test_ME_Val _ -> e::acc
                                   | Test_PN _ -> e::acc
                                   | Test_ER _ -> e::acc
                                   | Test_Int _ -> e::acc
                                   | Ref _ -> e::acc
                                   | Val _ -> e::acc
                                   | EnvironmentRecord _ -> e::acc
                                   | _ -> acc)
           ) ers []*)
  let o = (IntMap.find 11 es) in
  o.__HasProperty__(Str "blabla") s*)
  (exc_fun, (*lex_env, *)var_env, (*args_obj,*) o_gen,s._ExecutionContextStack_)*)
  (IntMap.find 4 excs,IntMap.find 24 os,IntMap.find 5 es,s._ExecutionContextStack_)

(*
let _ =
  let code = read_file "../JS_tests/test 2.js" in
  let (res,s) = initialState (InterpJs.runJS code) in
  res*)
  (*let (ers,_) = s._Objects_ in (res, IntMap.find 19 ers*)
