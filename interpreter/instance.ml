open Parser_flow
open Jskel

exception NotImplemented of string
exception Fail of string

(*----------------------- Interpretation Monad -----------------------*)

module M =  struct
  exception Branch_fail of string
  type 'a t = 'a
  let ret x = x
  let rec branch l =
    begin match l with
    | [] -> raise (Branch_fail "No branch matches")
    | b1 :: bq ->
       try b1 () with Branch_fail _ -> branch bq
    end
  let fail s = raise (Branch_fail s)
  let bind x f = f x
  let apply f x = f x
  let extract x = x
end

(*----------------------- Maps definition -----------------------*)
module IntMap = Map.Make (Int)
module StrMap = Map.Make(String)

(*----------------------- Unspecified Types definition -----------------------*)
module rec T : sig
  type bigInt = unit (*Ocaml Big_int*)
  and float = Float.t
  and int = Int.t
  and 'a intMap = 'a IntMap.t
  and 'a list = 'a List.t
  and string = String.t
  and sym = String.t
       end = T

(*----------------------- Unspecified Terms definition -----------------------*)
module Unspec = struct
  include Unspec(M)(T)

  let zero : float = Float.zero

  let _copyDataProperties_ : string = "copyDataProperties"

  let _GeneratorStart_ : string = "GeneratorStart"

  let _GeneratorResume_ : string = "GeneratorResume"

  let _GeneratorYield_ : string = "GeneratorYield"

  let _GeneratorValidate_ : string = "GeneratorValidate"

  let _bfo_Call_ : string = "bfo_Call"

  let _prova_ : string = "prova"

  let _createMappedArgumentsObject_ : string = "createMappedArgumentsObject"

  let _createIteratorFromClosure_ : string = "createIteratorFromClosure"

  let _iteratorStep_ : string = "iteratorStep"

  let __Generator_prototype_next__ : string = "_Generator_prototype_next_"

  let _createListIteratorRecord_closure_ : string = "createListIteratorRecord_closure"

  let _functionCalls_Evaluation_ : string = "functionCalls_Evaluation"

  let _prepareForOrdinaryCall_ : string = "prepareForOrdinaryCall"

  let _evaluateFunctionBody_ : string = "evaluateFunctionBody"

  let _evaluateConciseBody_ : string = "evaluateConciseBody"

  let _GeneratorStart_closure_sp_ : string = "GeneratorStart_closure_sp"

  let _GeneratorStart_closure_ : string = "GeneratorStart_closure"

  let _GeneratorYield_closure_ : string = "GeneratorYield_closure"

  let _singleNameBinding_IteratorBindingInitialization_ : string = "singleNameBinding_IteratorBindingInitialization"

  let wrongValueType : anomaly = StringAnomaly "The value has the wrong type"

  let updPropAnomaly : anomaly = StringAnomaly "An anomaly has occured while updating a property"

  let two_to_32_minus_1 : int =  int_of_float ((2. ** 32.) -. 1.)

  let table30 : internalSlot list = [] (*Temporary*)

  let space : string = " "

  let ordinaryToPrimitive_valueOf_toString_list : value list = [Str "valueOf";Str "toString"]

  let ordinaryToPrimitive_toString_valueOf_list : value list = [Str "toString";Str "valueOf"]

  let one : float = Float.one

  let notFoundAnomaly : anomaly = StringAnomaly "Location not found"

  let notFoundEnvVal : anomaly = StringAnomaly "Local Environment value not found"

  let new_locEnv : (any IntMap.t* int) = (IntMap.empty, 0)

  let excStkAnomaly : anomaly = StringAnomaly "Empty stack"

  let restoreContextAnomaly : anomaly = StringAnomaly "anomaly in restoring context"

  let optAppAnomaly : anomaly = StringAnomaly "impossible to apply"

  let bindingAnomaly : anomaly = StringAnomaly "ops, binding anomaly"

  let list_GeneratorState_GeneratorContext_GeneratorBrand_ : internalSlot list = [GeneratorState;GeneratorContext;GeneratorBrand]

  let list_SetPrototypeOf_IsExtensible_PreventExtensions : internalSlot list = [SetPrototypeOf;IsExtensible;PreventExtensions]

  let list_ProxyHandler_ProxyTarget_ : internalSlot list = [ProxyHandler;ProxyTarget]

  let list_Prototype_Extensible_StringData : internalSlot list = [Prototype;Extensible;StringData]

  let list_Prototype_Extensible_ParameterMap : internalSlot list = [Prototype;Extensible;ParameterMap]

  let list_Prototype_Extensible : internalSlot list = [Prototype;Extensible]

  let list_ParameterMap_ : internalSlot list = [ParameterMap]

  let list_GetPrototypeOf_SetPrototypeOf : internalSlot list = [GetPrototypeOf;SetPrototypeOf]

  let list_Name_Env_ : internalSlot list = [Name;Env]

  let intZero : int = Int.zero

  let intOne : int = Int.one

  let getPropAnomaly : anomaly = StringAnomaly "An anomaly has occured while getting a property"

  let foundNone : anomaly = StringAnomaly "The field has been found as VNone"

  let foundUndef : anomaly = StringAnomaly "The field has not been found MU_Undefined"

  let foundNull : anomaly = StringAnomaly "The field has not been found MN_Null"

  let foundEmpty : anomaly = StringAnomaly "The field has not been found Empty"

  let float__positiveZero : float = Float.zero

  let float__positiveInfinity : float = Float.infinity

  let float__negativeZero : float = -0.

  let float__negativeInfinity : float = Float.neg_infinity

  let float__NaN : float = Float.nan

  let _PositiveInfinity_ : string = "infinity"

  let _MakeArgGetter_ : string = "MakeArgGetter"

  let _NegativeInfinity_ : string = "-infinity"

  let _PositiveZero_ : string = "0"

  let _NegativeZero_ : string = "-0"

  let f_32 : float = 32.

  let f_2_8 : float = 2. ** 8.

  let f_2_7 : float = 2. ** 7.

  let f_2_32 : float = 2. ** 32.

  let f_2_31 : float = 2. ** 31.

  let f_2_16 : float = 2. ** 16.

  let f_2_15 : float = 2. ** 15.

  let f_2_ : float = 2.

  let emptyString : string = ""

  let delPropAnomaly : anomaly = StringAnomaly "An anomaly has occured while deleting a property"

  let bigInt__unit : _bigInt = BI_BigInt () (*Temporary*)

  let assertionTrueFailed : anomaly = StringAnomaly "assT failed"

  let assertionFalseFailed : anomaly = StringAnomaly "assF failed"

  let anomaly_Extract_Pure_Value_From_Empty : anomaly = StringAnomaly "maybeEmpty<a> to a conversion failed"

  let addPropAnomaly : anomaly = StringAnomaly "An anomaly has occured while adding a property"

  let aa_unscopables : value = Str "@@unscopables symbol. For now it is a string" (*Temporary*)

  let aa_toPrimitive : value = Str "@@toPrimitive symbol. For now it is a string" (*Temporary*)

  let aa_iterator : value = Str "@@iterator symbol. For now it is a string" (*Temporary*)

  let aa_hasInstance : value = Str "@@hasInstance symbol. For now it is a string" (*Temporary*)

  let aa_asyncIterator : value = Str "@@asyncIterator symbol. For now it is a string" (*Temporary*)

  let aa_toStringTag : value = Str "@@toStringTag symbol. For now it is a string" (*Temporary*)

  let _yield_ : string = "yield"

  let _writable_ : string = "writable"

  let _value_ : string = "value"

  let _undefined_ : string = "undefined"

  let _throw_ : string = "throw"

  let _Generator_ : string = "Generator"

  let _GeneratorFunction_ : string = "GeneratorFunction"

  let _unaryExpression_Evaluation_ : string = "unaryExpression_Evaluation"

  let _typeError_ : string = "typeError"

  let _true_ : string = "true"

  let _toString_ : string = "toString"

  let _toPropertyDescriptor_ : string = "toPropertyDescriptor"

  let _toPrimitive_ : string = "toPrimitive"

  let _toObject_ : string = "toString"

  let _toNumber_ : string = "toNumber"

  let _toLength_ : string =  "toLength"

  let _toBigInt_ : string = "ToBigInt"

  let _syntaxError_ : string = "syntaxError"

  let _symbol_ : string = "symbol"

  let _superCall_Evaluation_ : string = "superCall_Evaluation"

  let _string_ : string = "string"

  let _set_ : string = "set"

  let _setPrototypeOf_ : string = "setPrototypeOf"

  let _return_ : string = "return"

  let _requireObjectCoercible_ : string = "requireObjectCoercible"

  let _requireInternalSlot_ : string = "requireInternalSlot"

  let _relationalExpression_Evaluation_ : string = "relationalExpression_Evaluation"

  let _referenceError_ : string = "referenceError"

  let _rangeError_ : string = "rangeError"

  let _quote_ : string = "'"

  let _doubleQuote_ : string = "\""

  let _putValue_ : string = "putValue"

  let _proxy_Set_ : string = "proxy_Set"

  let _proxy_SetPrototypeOf_ : string = "proxy_SetPrototypeOf"

  let _proxy_PreventExtensions_ : string = "proxy_PreventExtensions"

  let _proxy_IsExtensible_ : string = "proxy_IsExtensible"

  let _proxy_HasProperty_ : string = "proxy_HasProperty"

  let _proxy_Get_ : string = "proxy_Get"

  let _proxy_GetPrototypeOf_ : string = "proxy_GetPrototypeOf"

  let _proxy_GetOwnProperty_ : string = "proxy_GetOwnProperty"

  let _proxy_Delete_ : string = "proxy_Delete"

  let _proxy_DefineOwnProperty_ : string = "proxy_DefineOwnProperty"

  let _proxy_Construct_ : string = "proxy_Construct"

  let _proxy_Call_ : string = "proxy_Call"

  let _proxyCreate_ : string = "proxyCreate"

  let _prototype_ : string = "prototype"

  let _preventExtensions_ : string = "preventExtensions"

  let _parseScript_ : string = "parseScript"

  let _ordinaryToPrimitive_ : string = "ordinaryToPrimitive"

  let _ordinaryHasInstance_ : string = "ordinaryHasInstance"

  let _openSquare_ : string = "["

  let _oer_SetMutableBinding_ : string = "oer_SetMutableBinding"

  let _oer_GetBindingValue_ : string = "oer_GetBindingValue"

  let _object_ : string = "object"

  let _number_ : string = "number"

  let _null_ : string = "null"

  let _next_ : string = "next"

  let _newExpression_EvaluateNew_ : string = "newExpression_EvaluateNew"

  let _name_ : string = "name"

  let _message_ : string = "message"

  let _memberExpression_EvaluateNew_ : string = "memberExpression_EvaluateNew"

  let _length_ : string = "length"

  let _iteratorNext_ : string = "iteratorNext"

  let _iteratorClose_ : string = "iteratorClose"

  let _isExtensible_ : string = "isExtensible"

  let _isArray_ : string = "isArray"

  let _instanceofOperator_ : string = "instanceofOperator"

  let _instanceOfOperator_ : string = "instanceOfOperator"

  let _has_ : string = "has"

  let _globalThis_ : string = "globalThis"

  let _globalDeclarationInstantiation_ : string = "globalDeclarationInstantiation"

  let _get_ : string = "get"

  let _getValue_ : string = "getValue"

  let _getPrototypeOf_ : string = "getPrototypeOf"

  let _getOwnPropertyDescriptor_ : string = "getOwnPropertyDescriptor"

  let _getMethod_ : string = "getMethod"

  let _getIterator_ : string = "getIterator"

  let _getFunctionRealm_ : string = "getFunctionRealm"

  let _ger_CreateMutableBinding_ : string = "ger_CreateMutableBinding"

  let _ger_CreateImmutableBinding_ : string = "ger_CreateImmutableBinding"

  let _function_ : string = "function"

  let _fer_GetThisBinding_ : string = "fer_GetThisBinding"

  let _fer_BindThisValue_ : string = "fer_BindThisValue"

  let _false_ : string = "false"

  let _evaluateCall_ : string = "evaluateCall"

  let _eval_ : string = "eval"

  let _evalError_ : string = "evalError"

  let _enumerable_ : string = "enumerable"

  let _efo_Construct_ : string = "efo_Construct"

  let _efo_Call_ : string = "efo_Call"

  let _done_ : string = "done"

  let _der_SetMutableBinding_ : string = "der_SetMutableBinding"

  let _der_GetBindingValue_ : string = "der_GetBindingValue"

  let _deleteProperty_ : string = "deleteProperty"

  let _deletePropertyOrThrow_ : string = "deletePropertyOrThrow"

  let _defineProperty_ : string = "defineProperty"

  let _definePropertyOrThrow_ : string = "definePropertyOrThrow"

  let _default_ : string = "default"

  let _createDataPropertyOrThrow_ : string = "createDataPropertyOrThrow"

  let _constructor_ : string = "constructor"

  let _construct_ : string = "construct"

  let _configurable_ : string = "configurable"

  let _closeSquare_ : string = "]"

  let _caller_ : string = "caller"

  let _callee_ : string = "callee"

  let _call_ : string = "call"

  let _boolean_ : string = "boolean"

  let _bigint_ : string = "bigint"

  let _await_ : string = "await"

  let _arraySetLength_ : string = "arraySetLength"

  let _arrayCreate_ : string = "arrayCreate"

  let _arguments_ : string = "arguments"

  let _apply_ : string = "apply"

  let _applyStringOrNumericBinaryOperator_ : string = "applyStringOrNumericBinaryOperator"

  let __prototype__ : string = "prototype"

  let _StringData_ : string = "StringData"

  let _NaN_ : string = "NaN"

  let _IntrinsicString_ : string = "%String%"

  let _IntrinsicStringPrototype_ : string = "%String.prototype%"

  let _IntrinsicObject_ : string = "%Object%"

  let _IntrinsicObjectPrototype_ : string = "%Object.prototype%"

  let _IntrinsicFunction_ : string = "%Function%"

  let _IntrinsicFunctionPrototype_ : string = "%Function.prototype%"

  let _Infinity_ : string = "Infinity"

  let _validateAndApplyPropertyDescriptor_ : string = "validateAndApplyPropertyDescriptor"

  let _updateEmpty_ : string = "updateEmpty"

  let _testIntegrityLevel_ : string = "testIntegrityLevel"

  let _setRealmGlobalObject_ : string = "setRealmGlobalObject"

  let _setIntegrityLevel_ : string = "setIntegrityLevel"

  let _setFunctionName_ : string = "setFunctionName"

  let _setFunctionLength_ : string = "setFunctionLength"

  let _seo_GetOwnProperty_ : string = "seo_GetOwnProperty"

  let _seo_DefineOwnProperty_ : string = "seo_DefineOwnProperty"

  let _sameValueNonNumeric_ : string = "sameValueNonNumeric"

  let _propertyDefinition_propertyDefinitionEvaluation_ : string = "propertyDefinition_propertyDefinitionEvaluation"

  let _ordinarySet_ : string = "ordinarySet"

  let _ordinarySetWithOwnDescriptor_ : string = "ordinarySetWithOwnDescriptor"

  let _ordinarySetPrototypeOf_ : string = "ordinarySetPrototypeOf"

  let _ordinaryHasProperty_ : string = "ordinaryHasProperty"

  let _ordinaryGet_ : string = "ordinaryGet"

  let _ordinaryGetOwnProperty_ : string = "ordinaryGetOwnProperty"

  let _ordinaryFunctionCreate_ : string = "ordinaryFunctionCreate"

  let _ordinaryDelete_ : string = "ordinaryDelete"

  let _ordinaryCallBindThis_ : string = "ordinaryCallBindThis"

  let _numeric__unsignedRightShift_ : string = "numeric__unsignedRightShift"

  let _numeric__subtract_ : string = "numeric__subtract"

  let _numeric__signedRightShift_ : string = "numeric__signedRightShift"

  let _numeric__sameValue_ : string = "numeric__sameValue"

  let _numeric__sameValueZero_ : string = "numeric__sameValueZero"

  let _numeric__reminder_ : string = "numeric__reminder"

  let _numeric__multiply_ : string = "numeric__multiply"

  let _numeric__lessThan_ : string = "numeric__lessThan"

  let _numeric__leftShift_ : string = "numeric__leftShift"

  let _numeric__exponentiate_ : string = "numeric__exponentiate"

  let _numeric__equal_ : string = "numeric__equal"

  let _numeric__divide_ : string = "numeric__divide"

  let _numeric__bitwiseXOR_ : string = "numeric__bitwiseXOR"

  let _numeric__bitwiseOR_ : string = "numeric__bitwiseOR"

  let _numeric__bitwiseAND_ : string = "numeric__bitwiseAND"

  let _numeric__add_ : string = "numeric__add"

  let _number__lessThan_ : string = "number__lessThan"

  let _newFunctionEnvironment_ : string = "newFunctionEnvironment"

  let _makeSuperPropertyReference_ : string = "makeSuperPropertyReference"

  let _makeMethod_ : string = "makeMethod"

  let _makeConstructor_ : string = "makeConstructor"

  let _makeClassConstructor_ : string = "makeClassConstructor"

  let _makeBasicObject_ : string = "makeBasicObject"

  let _iteratorValue_ : string = "iteratorValue"

  let _iteratorComplete_ : string = "iteratorComplete"

  let _isStringPrefix_ : string = "isStringPrefix"

  let _invoke_ : string = "invoke"

  let _instantiateOrdinaryFunctionExpression_ : string = "instantiateOrdinaryFunctionExpression"

  let _initializeReferencedBinding_ : string = "initializeReferencedBinding"

  let _hasProperty_ : string = "hasProperty"

  let _hasOwnProperty_ : string = "hasOwnProperty"

  let _getV_ : string = "getV"

  let _getThisValue_ : string = "getThisValue"

  let _getThisEnvironment_ : string = "getThisEnvironment"

  let _getSuperConstructor_ : string = "getSuperConstructor"

  let _getPrototypeFromConstructor_ : string = "getPrototypeFromConstructor"

  let _functionDeclarationInstantiation_ : string = "functionDeclarationInstantiation"

  let _fer_GetSuperBase_ : string = "fer_GetSuperBase"

  let _equalityExpression_Evaluation_ : string = "equalityExpression_Evaluation"

  let _der_InitializeBinding_ : string = "der_InitializeBinding"

  let _der_DeleteBinding_ : string = "der_DeleteBinding"

  let _der_CreateMutableBinding_ : string = "der_CreateMutableBinding"

  let _der_CreateImmutableBinding_ : string = "der_CreateImmutableBinding"

  let _createPerIterationEnvironment_ : string = "createPerIterationEnvironment"

  let _createMethodProperty_ : string = "createMethodProperty"

  let _createDataProperty_ : string = "createDataProperty"

  let _boundFunctionCreate_ : string = "boundFunctionCreate"

  let _blockDeclarationInstantiation_ : string = "blockDeclarationInstantiation"

  let _bfeo_Construct_ : string = "bfeo_Construct"

  let _assT_failed_ : string = "assert true failed in "

  let _assF_failed_ : string = "assert false failed in "

  let _argeo_Set_ : string = "argeo_Set"

  let _argeo_DefineOwnProperty_ : string = "argeo_DefineOwnProperty"

  let _aeo_DefineOwnProperty_ : string = "aeo_DefineOwnProperty"

  let _abstractRelationalComparison_ : string = "abstractRelationalComparison"

  let _val_to_MN_obj_ : string = "val_to_MN_obj"

  let _tryStatement_Evaluation_ : string = "tryStatement_Evaluation"

  let _templateLiteral_Evaluation_ : string = "templateLiteral_Evaluation"

  let _switchStatement_Evaluation_ : string = "switchStatement_Evaluation"

  let _regularExpressionLiteral_Evaluation_ : string = "regularExpressionLiteral_Evaluation"

  let _methodDefinition_MethodDefinitionEvaluation_ : string = "methodDefinition_MethodDefinitionEvaluation"

  let _memberExpression_Evaluation_ : string = "memberExpression_Evaluation"

  let _loc_Object_to_int_ : string = "loc_Object_to_int"

  let _declaration_Evaluation_ : string = "declaration_Evaluation"

  let _debuggerStatement_Evaluation_ : string = "debuggerStatement_Evaluation"

  let _2_53_minus_1 : int = int_of_float ((2. ** 53.) -. 1.)

  let _2_53 : int = int_of_float (2. ** 53.)

  let rec abs (f : float) : float M.t =
      M.ret (Float.abs f)

  and addPrefix_ ((s1 : string),(s2 : string)) : string M.t =
      M.ret (String.concat s1 [" ";s2])

  and betweenSquareBrackets (s : string) : string M.t =
      M.ret (String.concat "[" [s;"]"])

  and compareObject ((o1 : loc_Object),(o2: loc_Object)) : boolean M.t =
      M.ret (match o1 = o2 with
             |true -> T
             |false -> F)

  and compareString  ((s1 : string),(s2: string)) : boolean M.t =
      M.ret (match s1 = s2 with
             |true -> T
             |false -> F)

  and compareSymbol ((s1 : symbol),(s2: symbol)) : boolean M.t =
      M.ret (match s1 = s2 with
             |true -> T
             |false -> F)

  and fail (v : unit) : 'a M.t = raise (Fail "JSkel failure due to monads")

  and float__add ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.add f1 f2)

  and float__divide ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.div f1 f2)

  and float__equal ((f1 : float),(f2 : float)) : boolean M.t =
      M.ret (match Float.equal f1 f2 with
             |true -> T
             |false -> F)

  and float__exponentiate ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.pow f1 f2)

  and float__geq ((f1 : float),(f2 : float)) : boolean M.t =
      M.ret (match f1 >= f2 with
             |true -> T
             |false -> F)

  and float__greaterThan ((f1 : float),(f2 : float)) : boolean M.t =
      M.ret (match f1 > f2 with
             |true -> T
             |false -> F)

  and float__isInteger (v : float) : boolean M.t =
      M.ret (match Float.is_integer v with | true -> T | false -> F)

  and float__isZero (v : float) : boolean M.t =
      M.ret (match v = 0. with | true -> T | false -> F)

  and float__leftShift ((f1 : float),(f2 : float)) : float M.t =
      M.ret (float_of_int (Int.shift_left (int_of_float f1) (int_of_float f2)))

  and float__lessThan ((f1 : float),(f2 : float)) : boolean M.t =
      M.ret (match f1 < f2 with
             |true -> T
             |false -> F)

  and float__modulo ((f1 : float),(f2 : float)) : float M.t =
      M.ret (float_of_int (Int.rem (int_of_float f1) (int_of_float f2)))

  and float__multiply ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.mul f1 f2)

  and float__neg (f1 : float) : float M.t =
      M.ret (Float.mul f1 Float.minus_one)

  and float__of_int (i : int) : float M.t =
      M.ret (float_of_int i)

  and float__of_string (s : string) : float M.t =
      M.ret (float_of_string s)

  and float__reminder ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.rem f1 f2)

  and float__signedRightShift ((f1 : float),(f2 : float)) : float M.t =
      M.ret (float_of_int (Int.shift_right (int_of_float f1) (int_of_float f2)))

  and float__subtract ((f1 : float),(f2 : float)) : float M.t =
      M.ret (Float.sub f1 f2)

  and float__ToString (f : float) : string M.t =
      M.ret (string_of_float f)

  and float__unaryMinus (f : float) : float M.t =
      M.ret (Float.mul f (-1.))

  and float__unsignedRightShift ((f1 : float),(f2 : float)) : float M.t =
      M.ret (float_of_int (Int.shift_right_logical (int_of_float f1) (int_of_float f2)))

  and floor (f : float) : float M.t =
      M.ret (Float.floor f)

  and functionExpression_GetSource (fexp : functionExpression) : string M.t = M.ret "No function string available"

  and generateMessageError ((errorMessage: string), (meth: string)) : string M.t =
      M.ret (String.concat " " ["Has occured a";errorMessage;"in the method"; meth])

  and hd (l: 'a list) : 'a M.t =
      (match l with
      | h::t -> M.ret h
      | [] -> M.fail "the list is empty -> hd failed")

  and int__add ((i1 : int),(i2 :  int)) : int M.t =
      M.ret (Int.add i1 i2)

  and int__equal ((i1 : int),(i2 :  int)) : boolean M.t =
      M.ret (match Int.equal i1 i2 with
             |true -> T
             |false -> F)

  and int__geq ((i1 : int),(i2 :  int)) : boolean M.t =
      M.ret (match i1 >= i2 with
             |true -> T
             |false -> F)

  and int__leq ((i1 : int),(i2 :  int)) : boolean M.t =
      M.ret (match i1 <= i2 with
             |true -> T
             |false -> F)

  and int__lessThan ((i1 : int),(i2 :  int)) : boolean M.t =
      M.ret (match i1 < i2 with
             |true -> T
             |false -> F)

  and int__lognot (i : int) : int M.t =
      M.ret (Int.lognot i)

  and int__min ((i1 : int),(i2 :  int)) : int M.t =
      M.ret (match i1 >= i2 with
             |true -> i1
             |false -> i2)

  and int__neg (i : int) : int M.t=
      M.ret (Int.mul i Int.minus_one)

  and int__of_float (i : float) : int M.t =
      M.ret (int_of_float i)

  and int__subtract ((i1 : int),(i2 :  int)) : int M.t =
      M.ret (Int.sub i1 i2)

  and int__unaryMinus (i : int) : int M.t=
      M.ret (Int.add i Int.minus_one)

  and isArrayIndex (v : value) : boolean M.t=
    M.ret (match v with
           | Str s -> let x = (try
                                (Float.is_integer (float_of_string s))
                              with Failure _ -> false) in
                     (match x with
                     | true -> T
                     | false -> F)
           | _ -> F)

  and isEmptyString (s : string) : boolean M.t =
      M.ret (match s = "" with
             |true -> T
             |false -> F)

  and isStrictSource (s : string) : boolean M.t = M.ret F (*to modify in skel*)

  and list_append ((l : 'a list),(v : 'a)) : 'a list M.t =
      M.ret (List.append l [v])

  and list_append_inHead ((v : 'a), (l : 'a list)) : 'a list M.t =
      M.ret (List.append [v] l)

  and list_append_list ((l1 : 'a list),(l2 : 'a list)) : 'a list M.t =
      M.ret (l1@l2)

  and list_delete ((l : 'a list), (v :  'a)) : 'a list M.t =
      M.ret(let rec l_d (l,v) = match l with
              |[] -> []
              |h::t -> (match h = v with
                      |true -> l_d(t,v)
                      |false -> h::(l_d(t,v))) in
            l_d(l,v))

  and list_get_index ((l : 'a list), (index : int)) : 'a voption M.t =
      M.ret (match List.nth_opt l index with
             |None -> VNone
             |Some v -> VSome v)

  and list_hasDuplicates (l : 'a list) : boolean M.t =
      M.ret (let rec hasDup l =
               let rec member (t,h) =
                 (match t with
                 |[] -> F
                 |h'::t -> (match h = h' with
                          |true -> T
                          |false -> member(t, h))) in
               match l with
               |[] -> F
               |h::t -> (match member(t, h) with
                       |T -> T
                       |F -> hasDup t) in
             hasDup l)

  and list_isEmpty (l : 'a list) : boolean M.t =
      M.ret (match l with
             |[] -> T
             |h::t -> F)

  and list_isMember ((l : 'a list), (v : 'a)) : boolean M.t =
      M.ret (let rec member (t,h) =
                 (match t with
                 |[] -> F
                 |h'::t' -> (match h = h' with
                          |true -> T
                          |false -> member(t', h))) in
             member (l,v))

  and list_isNotMember ((l : 'a list), (v : 'a)) : boolean M.t =
      M.ret (let rec member (t,h) =
                 (match t with
                 |[] -> T
                 |h'::t' -> (match h = h' with
                          |true -> F
                          |false -> member(t', h))) in
             member (l,v))

  and list_len (l : 'a list) : int M.t =
      M.ret (let rec len l =
               match l with
               | [] -> Int.zero
               | h::t -> (Int.add Int.one (len t)) in
             len l)

  and list_reverse (l : 'a list) : 'a list M.t =
      M.ret (List.rev l)

  and map_add ((m : 'a intMap), (loc : int), (v : 'a)) : 'a intMap M.t =
      M.ret (IntMap.add loc v m)

  and map_read ((m : 'a intMap), (loc : int)) : 'a maybeEmpty M.t =
      M.ret (match IntMap.find_opt loc m with
             | Some v -> NotEmpty v
             | None -> Empty)

  and map_rmv ((m : 'a intMap), (loc : int)) : 'a intMap M.t =
      M.ret (IntMap.remove loc m)

  and map_update ((m : 'a intMap), (loc : int), (v : 'a)) : 'a intMap  M.t =
      M.ret (IntMap.add loc v m)

  and stack_empty (stck : 'a stack) : boolean M.t =
      M.ret (match stck with
             |[] -> T
             |h::t -> F)


  and stack_pop (stck : 'a stack) : ('a stack * 'a) maybeEmpty M.t =
      M.ret (match stck with
             | [] -> Empty
             | h::t -> (NotEmpty (t,h)))

  and stack_push ((stck: 'a stack), (v : 'a)) : 'a stack M.t =
      M.ret (v::stck)

  and stack_top (stck : 'a stack) : 'a maybeEmpty M.t =
      M.ret (match stck with
             | [] -> Empty
             | d::l -> (NotEmpty d))

  and str_isPrefix ((pref : string), (s : string)) : boolean M.t =
      M.ret (match pref = (String.sub s 0 ((String.length pref) - 1)) with
             |true -> T
             |false -> F)

  and string_concatenation (sl : string list) : string M.t =
      M.ret (String.concat "" sl)

  and string_length (s : string) : number M.t =
      M.ret (Float (float_of_int (String.length s)))

  and string__of_int (i : int) : string M.t =
      M.ret (string_of_int i)

  and tl (l : 'a list) : 'a list M.t =
      M.ret (List.tl l)

  and toList (v : 'a) : 'a list M.t = M.ret [v]

  and list_new : 'value list = []

  and list_new_empty : 'a list = []

  and intrinsic_function_default_properties :  objectProperty list =
      [{_PropertyKey_ = (Str "length");
        _Descriptor_ = { __Value__ = VSome (Numeric (Number (Float 1.)));
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome T;};
       };
       {_PropertyKey_ = (Str "prototype");
        _Descriptor_ = { __Value__ = VSome (Obj (ObjectLocation_Intrinsic IntrinsicFunctionPrototype));
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome F;};
       };
       {_PropertyKey_ = (Str "name");
        _Descriptor_ = { __Value__ = VSome (Str "Function");
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome F;}
       }
      ]

  and intrinsic_functionPrototype_default_properties :  objectProperty list =
    [{_PropertyKey_ = (Str "length");
      _Descriptor_ = { __Value__ = VSome (Numeric (Number (Float 0.)));
                       __Get__ = VNone;
                       __Set__ = VNone;
                       __Writable__ = VSome F;
                       __Enumerable__ = VSome F;
                       __Configurable__ = VSome T;};
     };
     {_PropertyKey_ = (Str "name");
      _Descriptor_ = { __Value__ = VSome (Str "");
                       __Get__ = VNone;
                       __Set__ = VNone;
                       __Writable__ = VSome F;
                       __Enumerable__ = VSome F;
                       __Configurable__ = VSome F;}
     }
    ]

  and intrinsic_object_default_properties :  objectProperty list =
      [{_PropertyKey_ = (Str "length");
        _Descriptor_ = { __Value__ = VSome (Numeric (Number (Float 1.)));
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome T;};
       };
       {_PropertyKey_ = (Str "name");
        _Descriptor_ = { __Value__ = VSome (Str "Object");
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome F;}
       }
      ]

  and intrinsic_objectPrototype_default_properties :  objectProperty list =
      []

  and intrinsic_string_default_properties :  objectProperty list =
      [{_PropertyKey_ =  (Str "prototype");
        _Descriptor_ =  { __Value__ = VSome (Obj (ObjectLocation_Intrinsic IntrinsicStringPrototype));
                                  __Get__ = VNone;
                                  __Set__ = VNone;
                                  __Writable__ = VSome F;
                                  __Enumerable__ = VSome F;
                                  __Configurable__ = VSome F;};
       };
       {_PropertyKey_ =  (Str "name");
        _Descriptor_ =  { __Value__ = VSome (Str "String");
                         __Get__ = VNone;
                         __Set__ = VNone;
                         __Writable__ = VSome F;
                         __Enumerable__ = VSome F;
                         __Configurable__ = VSome F;}
       }
      ]

  and intrinsic_stringPrototype_default_properties :  objectProperty list =
    [{_PropertyKey_ = (Str "length");
      _Descriptor_ = { __Value__ = VSome (Numeric (Number (Float 0.)));
                       __Get__ = VNone;
                       __Set__ = VNone;
                       __Writable__ = VSome F;
                       __Enumerable__ = VSome F;
                       __Configurable__ = VSome T;};
     };
    ]

  and parseText (s : string)  : script M.t =
  (*this is the parse that I need. I've to put this instead of the parse filter in the flow module*)
  M.ret(
    let parser = Parser_flow.program in
    let parse_options =
      Some
        Parser_env.
      {
        enums = false;
        esproposal_class_instance_fields = false;
        esproposal_class_static_fields = false;
        esproposal_decorators = false;
        esproposal_export_star_as = false;
        esproposal_optional_chaining = false;
        esproposal_nullish_coalescing = false;
        types = false;
        use_strict = false;
      } in
    let ((_, ast), _) = parser ~parse_options s ~fail:false  in
    process_program ast.Flow_ast.Program.statements)

(*Trasformation from Flow Parser output to JSkel synctactical representation of JS. Those act as interface between the FlowParser and the ES Syntax*)

  and process_Literal s =
    match s.Flow_ast.Literal.value with
    | Flow_ast.Literal.Number n -> (Lit_NumericLiteral n)
    | Flow_ast.Literal.String str -> (Lit_StringLiteral str)
    | Flow_ast.Literal.Boolean b -> (match b with
                                   | true -> (Lit_BooleanLiteral TrueLiteral)
                                   | false -> (Lit_BooleanLiteral FalseLiteral))
    | Flow_ast.Literal.Null -> (Lit_NullLiteral NullLiteral)
    | _ -> assert false

  and process_IdentifierReference s = match s with | (_, idRec) -> (IR_identifier (match idRec.Flow_ast.Identifier.name with |"Object" -> "%Object%" | _ -> idRec.name))

  and arrayElision_split l =
    let rec loop elisionList elemList =
      match elemList with
      | [] -> (elisionList,elemList)
      | hd::tl -> (match hd with
                | Flow_ast.Expression.Array.Hole _ -> loop (hd::elisionList) tl
                | _ -> (elisionList,elemList))
    in loop [] l

  and process_Elision l =
    match l with
    | [] -> assert false
    | hd::[] -> (E_Comma Comma)
    | hd::tl -> (E_Elision ((process_Elision tl), Comma))

  and process_ArrayLiteral s =
    match s with
    | (_, Flow_ast.Expression.Array aexp) ->
       (match aexp.elements with
       | [] -> (AL_Elision VNone)
       | hd::tl -> (match List.rev tl with
                 | [] -> (match hd with
                         | Flow_ast.Expression.Array.Hole _ -> (AL_Elision (VSome (E_Comma Comma)))
                         | _ -> (AL_ElementList (process_ElementList [hd])))
                 | hdF::tlF -> (match hdF with
                           | Flow_ast.Expression.Array.Hole _ -> let (elisionList,elemList) = arrayElision_split (hdF::tlF) in
                                                                (match (elemList,hd) with
                                                                 | ([],Flow_ast.Expression.Array.Hole _) ->
                                                                    let y = (VSome (process_Elision elisionList)) in
                                                                    (AL_Elision y)
                                                                 | ([], _) ->
                                                                    let y = (VSome (process_Elision elisionList)) in
                                                                    let x = process_ElementList [hd] in
                                                                    (AL_ElementListElision (x,y))
                                                                 | (l,Flow_ast.Expression.Array.Hole _) ->
                                                                    let y = (VSome (process_Elision elisionList)) in
                                                                    let x = process_ElementList (hd::(List.rev elemList)) in
                                                                    (AL_ElementListElision (x,y))
                                                                 | (l, _) ->
                                                                    let y = (VSome (process_Elision elisionList)) in
                                                                    let x = process_ElementList (hd::(List.rev elemList)) in
                                                                    (AL_ElementListElision (x,y)))
                           | _ -> (AL_ElementList (process_ElementList aexp.elements)))))
    | _ -> assert false

  and process_ElementList l =
    match l with
    | [] -> assert false
    | hd::tl -> (match hd with
              | Flow_ast.Expression.Array.Hole _ -> let (elisionListL,elemListL) = arrayElision_split l in
                                                   let elisionProd = process_Elision elisionListL in
                                                   (match elemListL with
                                                    | hdF::[] -> (match hdF with
                                                               | Flow_ast.Expression.Array.Expression exp -> (EL_ElisionAssignmentExpression (VSome elisionProd,(process_AssignmentExpression exp)))
                                                               | Flow_ast.Expression.Array.Spread sexp -> (EL_ElisionSpreadElement (VSome elisionProd,(process_SpreadElement sexp)))
                                                               | _ -> assert false)
                                                    | hdF::tlF -> (match List.rev elemListL with
                                                               | hdR::tlR -> (match hdR with
                                                                         | Flow_ast.Expression.Array.Expression exp ->
                                                                            let z = process_AssignmentExpression exp in
                                                                            let (elisionList,elemList) = arrayElision_split tlR in
                                                                            let y = (match elisionList with
                                                                                     | [] -> VNone
                                                                                     | _ -> VSome (process_Elision elisionList)) in
                                                                            let x = process_ElementList (elisionListL@(List.rev elemList)) in
                                                                            (EL_ElementListAssignmentExpression (x,y,z))
                                                                         | Flow_ast.Expression.Array.Spread sexp  ->
                                                                            let z = process_SpreadElement sexp in
                                                                            let (elisionList,elemList) = arrayElision_split tlR in
                                                                            let y = (match elisionList with
                                                                                     | [] -> VNone
                                                                                     | _ -> VSome (process_Elision elisionList)) in
                                                                            let x = process_ElementList (elisionListL@(List.rev elemList)) in
                                                                            (EL_ElementListSpreadElement (x,y,z))
                                                                         | _ -> assert false)
                                                               | _ -> assert false)
                                                    | _ -> assert false)
              | Flow_ast.Expression.Array.Expression exp -> (match tl with
                                                            | [] -> (EL_ElisionAssignmentExpression (VNone, (process_AssignmentExpression exp)))
                                                            | _ -> (match (List.rev tl) with
                                                                   | hdR::tlR -> (match hdR with
                                                                                | Flow_ast.Expression.Array.Expression exp ->
                                                                                   let (elisionList,elemList) = arrayElision_split (tlR) in
                                                                                   let z = (process_AssignmentExpression exp) in
                                                                                   let y = (match elisionList with
                                                                                            | [] -> VNone
                                                                                            | _ -> VSome (process_Elision elisionList)) in
                                                                                   let x = process_ElementList (hd::(List.rev elemList)) in (EL_ElementListAssignmentExpression (x,y,z))
                                                                                | Flow_ast.Expression.Array.Spread sexp ->
                                                                                   let (elisionList,elemList) = arrayElision_split (tlR) in
                                                                                   let y = (match elisionList with
                                                                                            | [] -> VNone
                                                                                            | _ -> VSome (process_Elision elisionList)) in
                                                                                   let x = process_ElementList (hd::(List.rev elemList)) in (EL_ElementListSpreadElement (x,y,(process_SpreadElement sexp)))
                                                                                | _ -> assert false)
                                                                   | _ -> assert false))
              | Flow_ast.Expression.Array.Spread sexp -> (match tl with
                                                         | [] -> (EL_ElisionAssignmentExpression (VNone, (process_SpreadElement sexp)))
                                                         | _ -> (match (List.rev tl) with
                                                                | hdR::tlR -> (match hd with
                                                                           | Flow_ast.Expression.Array.Expression exp ->
                                                                              let (elisionList,elemList) = arrayElision_split (tlR) in
                                                                              let y = (match elisionList with
                                                                                       | [] -> VNone
                                                                                       | _ -> VSome (process_Elision elisionList)) in
                                                                              let x = process_ElementList (hd::(List.rev elemList)) in (EL_ElementListAssignmentExpression (x,y,(process_AssignmentExpression exp)))
                                                                           | Flow_ast.Expression.Array.Spread sexp ->
                                                                              let (elisionList,elemList) = arrayElision_split (tlR) in
                                                                              let y = (match elisionList with
                                                                                       | [] -> VNone
                                                                                       | _ -> VSome (process_Elision elisionList)) in
                                                                              let x = process_ElementList (hd::(List.rev elemList)) in (EL_ElementListSpreadElement (x,y,(process_SpreadElement sexp)))
                                                                           | _ -> assert false)
                                                                | _ -> assert false)))
  and process_SpreadElement s =
    match s with
    | (_,sprRec) -> (process_AssignmentExpression sprRec.argument)

  and process_LiteralPropertyName s =
    match s with
    | Flow_ast.Expression.Object.Property.Identifier iexp -> (LPN_IdentifierName (match iexp with | (_, idRec) -> idRec.name))
    | Flow_ast.Expression.Object.Property.Literal (_,lexp) -> (match process_Literal lexp with
                                                              | Lit_StringLiteral sl -> (LPN_StringLiteral sl)
                                                              | Lit_NumericLiteral nl -> (LPN_NumericLiteral nl)
                                                              | _ -> assert false)
    | _ -> assert false

  and process_PropertyName s =
    match s with
    | Flow_ast.Expression.Object.Property.Computed (_, cexp) -> (PN_ComputedPropertyName (process_AssignmentExpression cexp.expression))
    | _ -> (PN_LiteralPropertyName (process_LiteralPropertyName s))

  and process_PropertyDefinition s =
    match s with
    | Flow_ast.Expression.Object.Property (_, Flow_ast.Expression.Object.Property.Init iexp) ->
       (match iexp.shorthand with
        | true -> (match iexp.key with
                  | Flow_ast.Expression.Object.Property.Identifier iexp' ->
                     (match iexp.value with
                     | (_,Flow_ast.Expression.Assignment _) -> (PD_CoverInitializedName ((process_IdentifierReference iexp'), (Init (process_AssignmentExpression iexp.value))))
                     | _ -> (PD_IdentifierReference (process_IdentifierReference iexp')))
                  | _ -> assert false)
        | false -> (PD_PropertyNameAssignmentExpression ((process_PropertyName iexp.key), (process_AssignmentExpression iexp.value))))
    | Flow_ast.Expression.Object.SpreadProperty (_,sexp) -> (PD_AssignmentExpression (process_AssignmentExpression sexp.argument))
    | Flow_ast.Expression.Object.Property (_, Flow_ast.Expression.Object.Property.Method _) -> (*PD_MethodDefinition (process_MethodDefinition (p,mexp)*) assert false
    | _ -> assert false

  and process_PropertyDefinitionList l =
    match l with
    | hd::[] -> (PDL_PropertyDefinition (process_PropertyDefinition hd))
    | hd::tl -> (match List.rev l with
              | hd::tl -> let y = process_PropertyDefinition hd in
                        let x = process_PropertyDefinitionList (List.rev tl) in
                        (PDL_PropertyDefinitionList (x,y))
              | _ -> assert false)
    | _ -> assert false

  and process_ObjectLiteral s =
    match s with
    | (_, Flow_ast.Expression.Object oexp) ->
       (match oexp.properties with
        | [] -> OL_Empty
        | hd::tl -> (OL_PropertyDefinitionList (process_PropertyDefinitionList oexp.properties))
        (*OL_PropertyDefinitionListComma is the same thing as the latter*))
    | _ -> assert false

  and process_FunctionExpression s =
    match s with
    | (_, Flow_ast.Expression.Function fexp) ->
       let id = (match fexp.id with
                 | None -> VNone
                 | Some (_,idRec) -> (VSome (process_BindingIdentifier idRec.name))) in
       let pars = process_FormalParameters fexp.params in
       let body = process_FunctionBody fexp.body in
       (id,pars,body)
    | _ -> assert false

  and process_PrimaryExpression s =
    match s with
    | (_, Flow_ast.Expression.Literal lit) -> (PE_Literal (process_Literal lit))
    | (_, Flow_ast.Expression.Identifier id) -> (PE_IdentifierReference (process_IdentifierReference id))
    | (_, Flow_ast.Expression.TaggedTemplate _) -> (PE_TemplateLiteral ())
    | (_, Flow_ast.Expression.TemplateLiteral _) -> (PE_TemplateLiteral ())
    | (_, Flow_ast.Expression.Array _) -> (PE_ArrayLiteral (process_ArrayLiteral s))
    | (_, Flow_ast.Expression.Object _) -> (PE_ObjectLiteral (process_ObjectLiteral s))
    | (_, Flow_ast.Expression.Function _) -> (PE_FunctionExpression (process_FunctionExpression s))
    | _ -> (PE_CoverParenthesizedExpressionAndArrowParameterList (CPEAAPL_Expression (process_expression s)))


  and process_MemberExpression s =
    match s with
    | (_, Flow_ast.Expression.Member mexp) ->
       (match mexp.property with
        | Flow_ast.Expression.Member.PropertyExpression exp ->
           (match mexp._object with
            | (_, Flow_ast.Expression.Identifier (_,iexp)) -> (match iexp.name = "super" with
                                                              | true -> let y = process_expression exp in (ME_SuperProperty (SP_Expression y))
                                                              | false -> let x = process_MemberExpression mexp._object in
                                                                        let y = process_expression exp in (ME_Expression (x,y)))
            | _ -> let x = process_MemberExpression mexp._object in let y = process_expression exp in (ME_Expression (x,y)))
        | Flow_ast.Expression.Member.PropertyIdentifier name ->
           (match mexp._object with
            | (_, Flow_ast.Expression.Identifier (_,iexp)) -> (match iexp.Flow_ast.Identifier.name = "super" with
                                                              | true -> let y = (match name with | (_, recName) -> recName.name) in (ME_SuperProperty (SP_IdentifierName y))
                                                              | false -> let x = process_MemberExpression mexp._object in
                                                                        let y = (match name with | (_, recName) -> recName.name) in (ME_IdentifierName (x,y)))
            | _ ->  let x = process_MemberExpression mexp._object in let y = (match name with | (_, recName) -> recName.name) in (ME_IdentifierName (x,y)))
        (*missing meta properties*)
        | _ -> (assert false))
    | (_, Flow_ast.Expression.TaggedTemplate texp) ->
       let x = process_MemberExpression texp.tag in
       let y = () (*error*)in
       (ME_TemplateLiteral (x,y))
    | _ -> (ME_PrimaryExpression (process_PrimaryExpression s))

  and process_Arguments s =
    (match s with
     | (_, args) ->
        (match args.Flow_ast.Expression.ArgList.arguments with
         | [] -> Args_Empty
         | hd::[] -> let x = process_ArgumentList [hd] in
                   (Args_ArgumentList x) (*cannot identify the comma case*)
         | hd::tl -> let x = process_ArgumentList args.arguments in
                   (Args_ArgumentList x)))

  and process_ArgumentList l =
    match l with
    | [] -> assert false
    | hd::[] -> (match hd with
               | (Flow_ast.Expression.Expression exp) -> (AL_AssignmentExpression (process_AssignmentExpression exp))
               | (Flow_ast.Expression.Spread sexp) -> (AL_DotsAssignmentExpression (process_AssignmentExpression (match sexp with
                                                                                                                 |(_,exp) -> exp.argument))))
    | hd::tl -> let revl = List.rev l in (match revl with
                                       | hd::tl -> (match hd with
                                                  | (Flow_ast.Expression.Expression exp) ->
                                                     let x = process_ArgumentList (List.rev tl) in
                                                     (AL_CommaAssignmentExpression (x, (process_AssignmentExpression exp)))
                                                  | (Flow_ast.Expression.Spread sexp) ->
                                                     let x = process_ArgumentList (List.rev tl) in
                                                     (AL_CommaDotsAssignmentExpression (x, (process_AssignmentExpression (match sexp with
                                                                                                                          |(_,exp) -> exp.argument)))))
                                       | _ -> assert false)

  and process_NewExpression s =
    (match s with
    | (_, Flow_ast.Expression.New nexp) ->
       (match nexp.arguments with
       | None -> (NE_NewExpression (process_NewExpression nexp.callee)) (*here we have also the last case of member expression.. a*sh***s*)
       | Some args -> (NE_MemberExpression (ME_NewMemberExpression ((process_MemberExpression nexp.callee), (process_Arguments args)))))
    | (_, Flow_ast.Expression.Member mexp) -> (NE_MemberExpression (process_MemberExpression s))
    | _ -> (NE_MemberExpression (process_MemberExpression s)))

  and process_LeftHandSideExpression s =
    match s with
    | (_, Flow_ast.Expression.Member mexp) -> (match mexp._object with
                                              | (_, Flow_ast.Expression.Call _) -> (LHSE_CallExpression (process_CallExpression (mexp._object, (Some mexp.property))))
                                              | (_, Flow_ast.Expression.Import _) -> (LHSE_CallExpression (process_CallExpression (mexp._object,(Some mexp.property))))
                                              | (_, Flow_ast.Expression.TaggedTemplate texp) ->
                                                 (match texp.tag with
                                                  | (_, Flow_ast.Expression.Call cexp) -> (LHSE_CallExpression (process_CallExpression(mexp._object,(Some mexp.property))))
                                                  | _ -> (LHSE_NewExpression (process_NewExpression s)))
                                              | _ -> (LHSE_NewExpression (process_NewExpression s)))
    | (_, Flow_ast.Expression.Call _) -> (LHSE_CallExpression (process_CallExpression (s,None)))
    | (_, Flow_ast.Expression.Import _) -> (LHSE_CallExpression (process_CallExpression (s,None)))
    | (_, Flow_ast.Expression.TaggedTemplate texp) ->
       (match texp.tag with
        | (_, Flow_ast.Expression.Call cexp) -> (LHSE_CallExpression (process_CallExpression(texp.tag,None)))
        | _ -> (LHSE_NewExpression (process_NewExpression s)))
    | (_, Flow_ast.Expression.OptionalCall _) -> (LHSE_OptionalExpression (process_OptionalExpression s))
    | (_, Flow_ast.Expression.OptionalMember _) -> (LHSE_OptionalExpression (process_OptionalExpression s))
    | _ -> (LHSE_NewExpression (process_NewExpression s))

  and process_OptionalExpression s =
    (match s with
     | (_, Flow_ast.Expression.OptionalMember oexp) ->
        (match oexp.member._object with
         | (_, Flow_ast.Expression.OptionalMember _) -> (OE_OptionalExpression ((process_OptionalExpression oexp.member._object), (process_OptionalChainMember oexp.member.property)))
         | (_, Flow_ast.Expression.OptionalCall _) -> (OE_OptionalExpression ((process_OptionalExpression oexp.member._object), (process_OptionalChainMember oexp.member.property)))
         | _ -> (OE_MemberExpression ((process_MemberExpression oexp.member._object),(process_OptionalChainMember oexp.member.property))))
     | (_, Flow_ast.Expression.OptionalCall oexp) ->
        (match oexp.call.callee with
         | (_, Flow_ast.Expression.OptionalMember _) -> (OE_OptionalExpression ((process_OptionalExpression oexp.call.callee), (process_OptionalChainCall oexp.call.arguments)))
         | (_, Flow_ast.Expression.OptionalCall _) -> (OE_OptionalExpression ((process_OptionalExpression oexp.call.callee), (process_OptionalChainCall oexp.call.arguments)))
         | _ -> (OE_CallExpression ((process_CallExpression (oexp.call.callee,None)),(process_OptionalChainCall oexp.call.arguments))))
     | _ -> assert false)

  and process_OptionalChainCall s =
    match s with
    | (_, argRec) -> (OC_QArguments (process_Arguments s))

  and process_OptionalChainMember s =
    match s with
    | Flow_ast.Expression.Member.PropertyExpression propExp -> (OC_QExpression (process_expression propExp))
    | Flow_ast.Expression.Member.PropertyIdentifier (p, name) -> (OC_QIdentifierName name.name)
    | _ -> assert false

  and process_CallExpression (s,properties) =
    match s with
    | (_, Flow_ast.Expression.Call cexp) ->
       (match properties with
        | None -> (match cexp.callee with
                  | (_, Flow_ast.Expression.Identifier (p,iexp)) -> (match iexp.name with
                                                           | "super" -> (CE_SuperCall (SC_Arguments (process_Arguments cexp.arguments)))
                                                           | _ ->  (CE_CoverCallExpressionAndAsyncArrowHead (CCEAAAH ((process_MemberExpression cexp.callee), (process_Arguments cexp.arguments)))))
                  | (p, Flow_ast.Expression.Call cexp') -> let x = (process_CallExpression ((p, Flow_ast.Expression.Call cexp'), None)) in
                                                          let y = (process_Arguments cexp.arguments) in (CE_Arguments (x,y))
                  | _ -> (CE_CoverCallExpressionAndAsyncArrowHead (CCEAAAH ((process_MemberExpression cexp.callee), (process_Arguments cexp.arguments)))))
        | Some properties -> (match properties with
                             | Flow_ast.Expression.Member.PropertyExpression properties ->
                                (CE_Expression ((process_CallExpression (s,None)),(process_expression properties)))
                             | Flow_ast.Expression.Member.PropertyIdentifier properties ->
                                (CE_IdentifierName ((process_CallExpression (s,None)), (match properties with | (_, recName) ->  recName.name)))
                             | _ -> assert false))
  | (_, Flow_ast.Expression.TaggedTemplate texp) ->
     (match texp.tag with
      | (_, Flow_ast.Expression.Call cexp) -> let x = (process_CallExpression (texp.tag,None)) in
                                             (*templateLiterals not implemented*)(CE_TemplateLiteral (x,()))
      | _ -> assert false)
  | (_, Flow_ast.Expression.Import iexp) -> (CE_ImportCall (IC_AssignmentExpression (process_AssignmentExpression iexp.argument)))
  | _ -> (CE_CoverCallExpressionAndAsyncArrowHead (CCEAAAH ((process_MemberExpression s), (Args_Empty))))

  and process_UpdateExpression s =
    match s with
    | (_, Flow_ast.Expression.Update uexp) ->
       (match (uexp.operator,uexp.prefix) with
        | (Flow_ast.Expression.Update.Increment,false) -> (UpdE_postInc (process_LeftHandSideExpression uexp.argument))
        | (Flow_ast.Expression.Update.Decrement,false) -> (UpdE_postDec (process_LeftHandSideExpression uexp.argument))
        | (Flow_ast.Expression.Update.Increment,true) -> (UpdE_preInc (process_UnaryExpression uexp.argument))
        | (Flow_ast.Expression.Update.Decrement,true) -> (UpdE_preDec (process_UnaryExpression uexp.argument)))
    | _ -> (UpdExp (process_LeftHandSideExpression s))

  and process_UnaryExpression s =
    match s with
    | (_, Flow_ast.Expression.Unary uexp) ->
       let x = process_UnaryExpression uexp.argument in
       (match uexp.operator with
        | Flow_ast.Expression.Unary.Minus -> (UnaE_minus x)
        | Flow_ast.Expression.Unary.Plus -> (UnaE_plus x)
        | Flow_ast.Expression.Unary.BitNot -> (UnaE_tilde x)
        | Flow_ast.Expression.Unary.Not -> (UnaE_bang x)
        | Flow_ast.Expression.Unary.Delete -> (UnaE_delete x)
        | Flow_ast.Expression.Unary.Void -> (UnaE_void x)
        | Flow_ast.Expression.Unary.Typeof -> (UnaE_typeof x)
        | _ -> assert false)
    | _ -> (UnaExp (process_UpdateExpression s))

  and process_ExponentiationExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       let x = process_UpdateExpression bexp.left in
       let y = process_ExponentiationExpression bexp.right in
       (match bexp.operator with
        | Flow_ast.Expression.Binary.Exp -> (EE_Exponentiation (x, y))
        | _ -> (EE_UnaryExpression (process_UnaryExpression s)))
    | _ -> (EE_UnaryExpression (process_UnaryExpression s))

  and process_MultiplicativeExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       (let x = process_MultiplicativeExpression bexp.left in
       let y = process_ExponentiationExpression bexp.right in
       match bexp.operator with
       | Flow_ast.Expression.Binary.Mult -> (MulE_Multiplicative (x, Multiply, y))
       | Flow_ast.Expression.Binary.Div -> (MulE_Multiplicative (x, Division, y))
       | Flow_ast.Expression.Binary.Mod -> (MulE_Multiplicative (x, Reminder, y))
       | _ -> (MulE_Exponentiation (process_ExponentiationExpression s)))
    | _ -> (MulE_Exponentiation (process_ExponentiationExpression s))

  and process_AdditiveExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       let x = process_AdditiveExpression bexp.left in
       let y = process_MultiplicativeExpression bexp.right in
        (match bexp.operator with
         | Flow_ast.Expression.Binary.Plus -> (AddE_Sum (x, y))
         | Flow_ast.Expression.Binary.Minus -> (AddE_Sub (x, y))
         | _ -> (AddE_Multiplicative (process_MultiplicativeExpression s)))
    | _ -> (AddE_Multiplicative (process_MultiplicativeExpression s))

  and process_ShiftExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       let x = process_ShiftExpression bexp.left in
       let y = process_AdditiveExpression bexp.right in
        (match bexp.operator with
         | Flow_ast.Expression.Binary.LShift -> (ShExp_LeftShift (x, y))
         | Flow_ast.Expression.Binary.RShift -> (ShExp_SignRightShift (x, y))
         | Flow_ast.Expression.Binary.RShift3 -> (ShExp_UnsignRightShift (x, y))
         | _ -> (ShExp_Additive (process_AdditiveExpression s)))
    | _ ->  (ShExp_Additive (process_AdditiveExpression s))

  and process_RelationalExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       let x = process_RelationalExpression bexp.left in
       let y = process_ShiftExpression bexp.right in
        (match bexp.operator with
         | Flow_ast.Expression.Binary.Instanceof -> (RelExp_instanceof (x, y))
         | Flow_ast.Expression.Binary.In -> (RelExp_in (x, y))
         | Flow_ast.Expression.Binary.LessThan -> (RelExp_lower (x, y))
         | Flow_ast.Expression.Binary.GreaterThan -> (RelExp_greater (x, y))
         | Flow_ast.Expression.Binary.LessThanEqual -> (RelExp_leq (x, y))
         | Flow_ast.Expression.Binary.GreaterThanEqual -> (RelExp_geq (x, y))
         | _ -> (RelExp_Shift (process_ShiftExpression s)))
    | _ ->  (RelExp_Shift (process_ShiftExpression s))

  and process_EqualityExpression s =
    match s with
    | (_, Flow_ast.Expression.Binary bexp) ->
       let x = process_EqualityExpression bexp.left in
       let y = process_RelationalExpression bexp.right in
       (match bexp.operator with
        | Flow_ast.Expression.Binary.Equal -> (EqExp_eq (x,y))
        | Flow_ast.Expression.Binary.NotEqual -> (EqExp_neq (x,y))
        | Flow_ast.Expression.Binary.StrictEqual -> (EqExp_strictEq (x,y))
        | Flow_ast.Expression.Binary.StrictNotEqual -> (EqExp_strictNeq (x,y))
        | _ -> (EqExp_Relational (process_RelationalExpression s)))
    | _ -> (EqExp_Relational (process_RelationalExpression s))

  and process_BitwiseANDExpression s =
    match s with
    | (_,Flow_ast.Expression.Binary bexp) ->
       let x = process_BitwiseANDExpression bexp.left in
       let y = process_EqualityExpression bexp.right in
        (match bexp.operator with
        | Flow_ast.Expression.Binary.BitAnd -> (BAE (x,y))
        | _ -> (BAE_Equality (process_EqualityExpression s)))
    | _ -> (BAE_Equality (process_EqualityExpression s))

  and process_BitwiseXORExpression s =
    match s with
    | (_,Flow_ast.Expression.Binary bexp) ->
       let x = process_BitwiseXORExpression bexp.left in
       let y = process_BitwiseANDExpression bexp.right in
        (match bexp.operator with
        | Flow_ast.Expression.Binary.Xor -> (BXE (x,y))
        | _ -> (BXE_BAE (process_BitwiseANDExpression s)))
    | _ -> (BXE_BAE (process_BitwiseANDExpression s))

  and process_BitwiseORExpression s =
    match s with
    | (_,Flow_ast.Expression.Binary bexp) ->
       let x = process_BitwiseORExpression bexp.left in
       let y = process_BitwiseXORExpression bexp.right in
        (match bexp.operator with
        | Flow_ast.Expression.Binary.BitOr -> (BOE (x,y))
        | _ -> (BOE_BXE (process_BitwiseXORExpression s)))
    | _ -> (BOE_BXE (process_BitwiseXORExpression s))

  and process_LogicalANDExpression s =
    match s with
    | (_,Flow_ast.Expression.Logical lexp) ->
       let x = process_LogicalANDExpression lexp.left in
       let y = process_BitwiseORExpression lexp.right in
       (match lexp.operator with
        | Flow_ast.Expression.Logical.And -> (LAE (x,y))
        | _ -> (LAE_BOR (process_BitwiseORExpression s)))
    | _ -> (LAE_BOR (process_BitwiseORExpression s))

  and process_LogicalORExpression s =
    match s with
    | (_, Flow_ast.Expression.Logical lexp) ->
       let x = process_LogicalORExpression lexp.left in
       let y = process_LogicalANDExpression lexp.right in
       (match lexp.operator with
            | Flow_ast.Expression.Logical.Or -> (LOE (x,y))
            | _ -> (LOE_LAE (process_LogicalANDExpression s)))
    | _ -> (LOE_LAE (process_LogicalANDExpression s))

  and process_ShortCircuitExpression s =
    match s with
    | (_, Flow_ast.Expression.Logical lexp) ->
      (match lexp.operator with
      | Flow_ast.Expression.Logical.NullishCoalesce -> (SCE_CoaE (process_CoalescenceExpression lexp))
      | _ -> (SCE_LOE (process_LogicalORExpression s)))
    | _ -> (SCE_LOE (process_LogicalORExpression s))

  and process_CoalescenceExpression s =
       (match s.operator with
        | Flow_ast.Expression.Logical.NullishCoalesce ->
           let x = process_CoalescenceExpressionHead s.left in
           let y = process_BitwiseORExpression s.right in
           (x,y)
        | _ -> assert false)

  and process_CoalescenceExpressionHead s =
    match s with
    | (_, Flow_ast.Expression.Logical lexp) ->
      (match lexp.operator with
      | Flow_ast.Expression.Logical.NullishCoalesce -> (CoaEH_CoaE (process_CoalescenceExpression lexp))
      | _ -> (CoaEH_BOR (process_BitwiseORExpression s)))
    | _ -> (CoaEH_BOR (process_BitwiseORExpression s))

  and process_ConditionalExpression s =
    match s with
    | (_,Flow_ast.Expression.Conditional cexp) ->
       let x = process_ShortCircuitExpression cexp.test in
       let y = process_AssignmentExpression cexp.consequent in
       let z = process_AssignmentExpression cexp.alternate in
       (CondE (x,y,z))
    | _ -> (CondE_ShortCircuit (process_ShortCircuitExpression s))

  and process_AssignmentExpression s =
    match s with
    | (_, Flow_ast.Expression.Assignment assExp) ->
       let x = (match assExp.left with
                 | (p, Flow_ast.Pattern.Identifier id) ->
                    let name = (match id.Flow_ast.Pattern.Identifier.name with
                                | (_, idStr) -> idStr.Flow_ast.Identifier.name) in
                    (LHSE_NewExpression (NE_MemberExpression (ME_PrimaryExpression (PE_IdentifierReference (IR_identifier  (match name with | "Object" -> "%Object%" | _ -> name))))))
                 | (p, Flow_ast.Pattern.Expression pexp) -> (process_LeftHandSideExpression pexp)
                 (*| (p, Flow_ast.Pattern.Object oexp) -> (process_LeftHandSideExpression (p, Flow_ast.Expression.Object oexp)) pattern to implement*)
                 | _ -> assert false) in
       let y = process_AssignmentExpression assExp.right in
       (match assExp.operator with
         | None -> (AssE_Assignment (x, y))
         | _ -> assert false)
    | _ -> (AssE_CondE (process_ConditionalExpression s))

  and process_expression s =
    match s with
    | (p, Flow_ast.Expression.Sequence sexp) ->
       let rec loop l =
         match l.Flow_ast.Expression.Sequence.expressions with
         | [] -> (assert false) (*if this case, then it should be a normal expression*)
         | hd::[] -> process_expression hd
         | hd::tl -> let x = process_expression hd in
                   let y = process_AssignmentExpression (p, Flow_ast.Expression.Sequence {sexp with expressions = tl}) in
                   (Expression (x,y))
       in loop sexp
    | _ -> (Expr_AssExpr (process_AssignmentExpression s))

  and process_VariableBindingID id =
    match id with
    | (_, id) -> let id = id.Flow_ast.Identifier.name in (BI_identifier  id)

  and process_VariableDeclarationBindingID (bi, i) =
    match (bi, i) with
    |((_, Flow_ast.Pattern.Identifier id), None) -> ((process_VariableBindingID id.name), VNone)
    |((_, Flow_ast.Pattern.Identifier id), Some exp) -> ((process_VariableBindingID id.name), (VSome (Init (process_AssignmentExpression exp))))
    | (_, _) -> assert false

  and process_lexicalDeclarationBindingID (bi, i) =
    match (bi, i) with
    |((_, Flow_ast.Pattern.Identifier id), None) -> ((process_VariableBindingID id.name), VNone)
    |((_, Flow_ast.Pattern.Identifier id), Some exp) -> ((process_VariableBindingID id.name), (VSome (Init (process_AssignmentExpression exp))))
    | (_, _) -> assert false

  and process_VariableDeclaration vd =
    match vd with
    | (_, vd) -> (VarDecl_ID (process_VariableDeclarationBindingID ((vd.Flow_ast.Statement.VariableDeclaration.Declarator.id), (vd.init))))

  and process_LexicalBinding lb =
    match lb with
    | (_, lb) -> (LexBind_ID (process_lexicalDeclarationBindingID ((lb.Flow_ast.Statement.VariableDeclaration.Declarator.id), (lb.init))))

  and process_VariableStatementList vsl =
    match vsl with
    | h::[] -> (VarDeclLST_Decl (process_VariableDeclaration h))
    | h::t -> (VarDeclLST ((process_VariableStatementList t),  (process_VariableDeclaration h)))
    | _ -> assert false

  and process_BindingList bl =
    match bl with
    | h::[] -> (LexBind (process_LexicalBinding h))
    | h::t -> (BindLST (process_BindingList t,  (process_LexicalBinding h)))
    | _ -> assert false

  and process_LexicalDeclaration lb = (process_BindingList lb.Flow_ast.Statement.VariableDeclaration.declarations)


  and process_VariableStatement s =  (process_VariableStatementList s.Flow_ast.Statement.VariableDeclaration.declarations)

  and process_IfStatement s =
      let x = (process_expression s.Flow_ast.Statement.If.test) in
      let y = (process_statement s.consequent) in
      (match s.alternate with
      | None -> (If (x,y))
      | Some (_,stmt)-> (IfElse (x,y,(process_statement stmt.Flow_ast.Statement.If.Alternate.body))))

  and process_BlockStatement s =
      match s.Flow_ast.Statement.Block.body with
      | [] -> (BlockSTMT (Block (VNone)))
      | _ -> (BlockSTMT (Block (VSome (process_statement_list (List.rev s.body)))))

  and process_ReturnStatement s =
      match s.Flow_ast.Statement.Return.argument with
      | None -> (RetSTMT)
      | Some exp -> (RetSTMT_ID (process_expression exp))

  and process_LabelIdentifier s =
    match s with
    | "yield" -> (LI_yield)
    | "await" -> (LI_await)
    | _ -> (LI_identifier s)

  and process_ContinueStatement s =
    match s.Flow_ast.Statement.Continue.label with
    | None -> (ContSTMT)
    | Some (_,idRec) -> (ContSTMT_ID (process_LabelIdentifier idRec.name))

  and process_BreakStatement s =
    match s.Flow_ast.Statement.Break.label with
    | None -> (BreakSTMT)
    | Some (_,idRec) -> (BreakSTMT_ID (process_LabelIdentifier idRec.name))

  and process_WithStatement s =
    let x = process_expression s.Flow_ast.Statement.With._object in
    let y = process_statement s.body in
    (x,y)

  and process_FunctionDeclaration s = assert false

  and process_LabelledItem s =
    match s with
    | (_, Flow_ast.Statement.FunctionDeclaration _) -> (LabIt_FunDec (process_FunctionDeclaration s))
    | _ -> (LabIt_STMT (process_statement s))

  and process_LabelledStatement s =
    let x = (match s.Flow_ast.Statement.Labeled.label with
             | (_, idRec) -> process_LabelIdentifier idRec.name) in
    let y = process_LabelledItem s.body in
    (x,y)

  and process_ThrowStatement s =
    process_expression s.Flow_ast.Statement.Throw.argument

  and process_IterationStatement s =
    match s with
    | (_, Flow_ast.Statement.DoWhile dwexp) ->
       let x = process_statement (dwexp.body) in
       let y = process_expression (dwexp.test) in
       (DoWhile (x,y))
    | (_, Flow_ast.Statement.While wexp) ->
       let x = process_expression (wexp.test) in
       let y = process_statement (match wexp.body with | (_,Flow_ast.Statement.DoWhile dwexp) -> dwexp.body | _ -> assert false) in
       (While (x,y))
    | _ -> assert false

  and process_SwitchStatement s =
    assert false
  (*
    match s with
    | (_, Flow_ast.Statement.Switch sexp) ->
       let x = process_expression (sexp.Flow_ast.Statement.Switch.discriminant) in
       let y = process_CaseBlock (sexp.cases) in
   *)

  and process_statement s =
    match s with
    | (_, Flow_ast.Statement.Empty _) -> (EmptyStatement ())
    | (_, Flow_ast.Statement.Expression expr) -> (ExpressionStatement (process_expression expr.expression))
    | (_, Flow_ast.Statement.If ifexpr) -> (IfStatement (process_IfStatement ifexpr))
    | (_, Flow_ast.Statement.Block bexp) -> (BlockStatement (process_BlockStatement bexp))
    | (_, Flow_ast.Statement.Return rexp) -> (ReturnStatement (process_ReturnStatement rexp))
    | (_, Flow_ast.Statement.Continue cexp) -> (ContinueStatement (process_ContinueStatement cexp))
    | (_, Flow_ast.Statement.Break bexp) -> (BreakStatement (process_BreakStatement bexp))
    | (_, Flow_ast.Statement.With wexp) -> (WithStatement (process_WithStatement wexp))
    | (_, Flow_ast.Statement.Labeled lexp) -> (LabelledStatement (process_LabelledStatement lexp))
    | (_, Flow_ast.Statement.Throw texp) -> (ThrowStatement (process_ThrowStatement texp))
    | (_, Flow_ast.Statement.DoWhile _) -> (BreakableStatement (IterationStatement (process_IterationStatement s)))
    | (_, Flow_ast.Statement.While _) -> (BreakableStatement (IterationStatement (process_IterationStatement s)))
    | (_, Flow_ast.Statement.For _) -> (BreakableStatement (IterationStatement (process_IterationStatement s)))
    | (_, Flow_ast.Statement.ForIn _) -> (BreakableStatement (IterationStatement (process_IterationStatement s)))
    | (_, Flow_ast.Statement.ForOf _) -> (BreakableStatement (IterationStatement (process_IterationStatement s)))
    | (_, Flow_ast.Statement.Switch _) -> (BreakableStatement (SwitchStatement (process_SwitchStatement s)))
    | (_, _) -> assert false

  and process_BindingIdentifier s =
    match s with
    | "yield" -> (BI_yield)
    | "await" -> (BI_await)
    | _ -> (BI_identifier s)

  and process_BindingRestElement s =
    match s with
    | (_, Flow_ast.Pattern.Identifier idRec) ->
       (match idRec.name with
        | (_,idRec) -> (BRE_BID (process_BindingIdentifier idRec.name)))
    | _ -> assert false

  and process_FunctionRestParameter s =
    match s with
    | (_, restRec) -> (process_BindingRestElement (restRec.Flow_ast.Function.RestParam.argument))

  and process_SingleNameBinding (arg,def) =
    match (arg,def) with
    | ((_, Flow_ast.Pattern.Identifier idRec), None) ->
       (match idRec.name with
        | (_,idRec) -> (process_BindingIdentifier idRec.name, VNone))
    | ((_, Flow_ast.Pattern.Identifier idRec), Some def) ->
       (match idRec.name with
        | (_,idRec) -> (process_BindingIdentifier idRec.name, (VSome (Init (process_AssignmentExpression def)))))
    | _ -> assert false (*pattern case*)

  and process_BindingElement (arg,def) =
    match arg with
    | (_, Flow_ast.Pattern.Identifier _) -> (BindE_SingleName (process_SingleNameBinding (arg,def)))
    | _ -> assert false (*pattern case*)

  and process_FormalParameter (arg,def) =
    (FormPar (process_BindingElement (arg,def)))

  and process_FormalParameterList s =
    match List.rev s with
    | hd::[] ->
       (match hd with
        | (_, parRec) -> (FormParList_Par (process_FormalParameter (parRec.Flow_ast.Function.Param.argument, parRec.default))))
    | hd::tl ->
       (match hd with
       |(_, parRec) -> let x = process_FormalParameter (parRec.Flow_ast.Function.Param.argument, parRec.default) in
                      let y = process_FormalParameterList (List.rev tl) in
                      (FormParList (y, x)))
    | _ -> assert false

  and process_FormalParameters s =
    match s with
    | (_, paramRec) ->
       (match (paramRec.Flow_ast.Function.Params.params, paramRec.rest) with
        | ([],None) -> FormPars_Empty
        | ([], (Some restPar)) -> (FormPars_RestParam (process_FunctionRestParameter restPar))
        | (hd::tl, None) -> (FormPars_List (process_FormalParameterList (paramRec.params)))
        | (hd::tl, Some restPar) -> (FormPars ((process_FormalParameterList (paramRec.params)), (process_FunctionRestParameter restPar))))

  and process_FunctionBody s =
    match s with
    | Flow_ast.Function.BodyBlock (_,bodyRec) ->
       (match (List.rev bodyRec.body) with
        | [] -> (FuncBody VNone)
        | _ -> (FuncBody (VSome (process_statement_list (List.rev bodyRec.body)))))
    | _ -> assert false


  and process_statementListItem s =
    match s with
    | (_, Flow_ast.Statement.VariableDeclaration vs) -> (match vs.kind with
                                                         | Flow_ast.Statement.VariableDeclaration.Var -> (STMTListIt_STMT (VariableStatement (process_VariableStatement vs)))
                                                         | Flow_ast.Statement.VariableDeclaration.Let -> (STMTListIt_Decl (LexicalDeclaration (Let, process_LexicalDeclaration vs)))
                                                         | Flow_ast.Statement.VariableDeclaration.Const -> (STMTListIt_Decl (LexicalDeclaration (Const, process_LexicalDeclaration vs)))) (*to be checked*)
    | (_, Flow_ast.Statement.FunctionDeclaration fd) ->
       (match fd.Flow_ast.Function.id with
        | None -> let x = process_FormalParameters fd.params in
                 let y = process_FunctionBody fd.body in
                 (STMTListIt_Decl (HoistableDeclaration (FunctionDeclaration (FuncDecl (x,y)))))
        | Some (_, id) -> let x = process_BindingIdentifier id.name in
                         let y = process_FormalParameters fd.params in
                         let z = process_FunctionBody fd.body in
                         (STMTListIt_Decl (HoistableDeclaration (FunctionDeclaration (FuncDecl_Bound (x,y,z))))))
    | (_, _) -> (STMTListIt_STMT (process_statement s))

  and process_statement_list = function
    | s::[] ->  (STMTList_Item (process_statementListItem s))
    | s :: ss -> (STMTList (process_statement_list ss, process_statementListItem s))
    | _ -> assert false

  and process_program p =
    let sl = List.rev p in
    match sl with
    | [] ->  VNone
    | h::t -> (VSome (ScriptBody (process_statement_list (h::t))))

end

module InterpJs = MakeInterpreter(Unspec)
